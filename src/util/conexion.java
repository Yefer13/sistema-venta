package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class conexion
{
	public EntityManager em;
	public EntityManagerFactory emf;
	
	public void abrir_conexion() throws Exception
	{
		emf = Persistence.createEntityManagerFactory("SistemaVentas2020");
		em = emf.createEntityManager();
	}
	
	public void cerrar_conexion() throws Exception
	{
		emf.close();
		em.close();
	}
}
