package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.cargo;
import modelo.categoria;
import modelo.servicios;
import services.cargoService;
import services.categoriaService;
import services.serviciosService;
@FacesConverter("serviciosConverter")
public class serviciosConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		serviciosService serServ = new serviciosService();
		servicios ser = null;
		try 
		{
			ser = serServ.buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ser;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((servicios)value).getIdServicios());
	}
	
}
