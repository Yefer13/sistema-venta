package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.cargo;
import modelo.categoria;
import services.cargoService;
import services.categoriaService;
@FacesConverter("cargoConverter")
public class cargoConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		cargoService carserv = new cargoService();
		cargo car = null;
		try {
			car = carserv.buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return car;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((cargo)value).getIdCargo());
	}
	
}
