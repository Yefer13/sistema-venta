package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.proveedor;
import services.proveedorService;
@FacesConverter("proveedorConverter")
public class proveedorConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		proveedorService proserv = new proveedorService();
		proveedor prov = null;
		try {
			prov = proserv.buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prov;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((proveedor)value).getIdProveedor());
	}
	
}
