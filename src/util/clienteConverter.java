package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import modelo.cliente;
import services.clienteService;
@FacesConverter("clienteConverter")
public class clienteConverter implements Converter 
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		clienteService cliserv = new clienteService();
		cliente cli = null;
		try 
		{
			cli = cliserv.buscar(value);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return cli;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((cliente)value).getIdCliente());
	}
	
}
