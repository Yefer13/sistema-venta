package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.categoria;
import services.categoriaService;
@FacesConverter("categoriaConverter")
public class categoriaConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		categoriaService catserv = new categoriaService();
		categoria cat = null;
		try {
			cat = catserv.buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cat;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((categoria)value).getIdCategoria());
	}
	
}
