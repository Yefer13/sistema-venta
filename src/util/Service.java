package util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.bean.ViewScoped;
public class Service 
{	
	public byte[] getProductImage(String idProducto) throws IOException,SQLException 
	{
		Connection con = null;
		PreparedStatement stmt = null;
		byte[] img = null;
		
		try 
		{
			Class.forName("org.mariadb.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mariadb://localhost:3306/ventas", "root", "130912");
		}
		catch (Exception e)
		{
			System.out.println(e);
			System.exit(0);
		}
		
		stmt = con.prepareStatement("select * from producto where idProducto=?");
		stmt.setString(1, idProducto);
		ResultSet rs = stmt.executeQuery();
		
		while (rs.next()) {
			img = rs.getBytes("img");
		}
		
		rs.close();
		con.close();
		
		return img;
		}
	}
