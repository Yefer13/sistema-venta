package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.cargo;
import modelo.categoria;
import modelo.producto;
import services.cargoService;
import services.categoriaService;
import services.productoService;
@FacesConverter("productoConverter")
public class productoConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		productoService proServ = new productoService();
		producto pro = null;
		try 
		{
			pro = proServ.buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pro;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((producto)value).getIdProducto());
	}
	
}
