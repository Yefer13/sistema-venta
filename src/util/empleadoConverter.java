package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.datos;
import modelo.empleado;
import services.empleadoService;
@FacesConverter("empleadoConverter")
public class empleadoConverter implements Converter
{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) 
	{
		empleadoService empServ = new empleadoService();
		empleado emp = null;
		try 
		{
			emp = empServ.buscar(value);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) 
	{
		return String.valueOf(((empleado)value).getIdEmpleado());
	}
	
}
