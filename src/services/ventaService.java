package services;

import java.util.List;

import daoImplement.ventaDaoImpl;
import modelo.cliente;
import modelo.detalleVenta;
import modelo.venta;

public class ventaService extends ventaDaoImpl
{
	@Override
	public boolean registrar(venta v) throws Exception {
		// TODO Auto-generated method stub
		return super.registrar(v);
	}
	
	@Override
	public void actualizar(venta v) throws Exception {
		// TODO Auto-generated method stub
		super.actualizar(v);
	}
	
	@Override
	public List<venta> listar(venta v) throws Exception {
		// TODO Auto-generated method stub
		return super.listar(v);
	}
	
	@Override
	public List<venta> lista() throws Exception {
		// TODO Auto-generated method stub
		return super.lista();
	}
	
	@Override
	public List<venta> listaCP(cliente cli) throws Exception {
		// TODO Auto-generated method stub
		return super.listaCP(cli);
	}
	
	@Override
	public void actualizarDet(detalleVenta d) throws Exception {
		// TODO Auto-generated method stub
		super.actualizarDet(d);
	}
	
	@Override
	public boolean actualizarV(venta v) throws Exception {
		// TODO Auto-generated method stub
		return super.actualizarV(v);
	}
	
	@Override
	public boolean eliminar(venta v) throws Exception {
		// TODO Auto-generated method stub
		return super.eliminar(v);
	}
	
	@Override
	public Long obtenerTotalRegistro() throws Exception {
		// TODO Auto-generated method stub
		return super.obtenerTotalRegistro();
	}
	
	@Override
	public venta obtenerUltimoRegistro() throws Exception {
		// TODO Auto-generated method stub
		return super.obtenerUltimoRegistro();
	}
	
	@Override
	public List<venta> listaTotalRegistro() throws Exception {
		// TODO Auto-generated method stub
		return super.listaTotalRegistro();
	}
	
	@Override
	public List<venta> listaUltimoRegistro() throws Exception {
		// TODO Auto-generated method stub
		return super.listaUltimoRegistro();
	}
}
