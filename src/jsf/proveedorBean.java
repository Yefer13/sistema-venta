package jsf;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.comunicacion;
import modelo.proveedor;
import modelo.ubicacion;
import services.proveedorService;

@ManagedBean(name="proveedorBean")
@SessionScoped
@ViewScoped
public class proveedorBean
{
	private proveedor proR;
	private proveedor proA;
	private comunicacion com;
	private ubicacion ubi;
	private proveedorService proService = new proveedorService();
	private List<proveedor> listaProveedor;
	private int tamano_lista;
	private boolean accion;
	
	public proveedorBean() throws Exception
	{
		proR = new proveedor();
		proA = new proveedor();
		com = new comunicacion();
		ubi = new ubicacion();
		listar();
	}
	
	public void registrar() throws Exception
	{
		proR.setTblComunicacion(com);
		com.setTblProveedor(proR);
		
		proR.setTblUbicacion(ubi);
		ubi.setTblProveedor(proR);
		
		proR.setFechaRegistro(new Date());
		
		if (proService.registrar(proR))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Proveedor " + proR.getRazonSocial() + " Registrado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Proveedor " + proR.getRazonSocial() + " No Registrado",""));
		}
		
		proR = new proveedor();
		com = new comunicacion();
		ubi = new ubicacion();
		listar();
	}
	
	public void actualizar() throws Exception
	{
		if (proService.actualizar(proA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Proveedor " + proA.getRazonSocial() + " Actualizado",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Proveedor " + proA.getRazonSocial() + " No Actualizado",""));
		}
		proA = new proveedor();
		com = new comunicacion();
		ubi = new ubicacion();
		listar();
	}
	
	public void listar() throws Exception
	{
		listaProveedor = proService.lista(proA);
		tamano_lista = listaProveedor.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay proveedores / Registre un proveedor",""));
			setAccion(false);
		} 
		else
		{
			setAccion(true);
		}
	}
	
	public void seleccionarProveedor(proveedor pro) throws Exception
	{
		this.proA = pro;
	}
	
	public List<proveedor> listaBuscar(String p) throws Exception
	{
		return proService.listaP(p);
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}

	public proveedor getProR() {
		return proR;
	}

	public void setProR(proveedor proR) {
		this.proR = proR;
	}

	public proveedor getProA() {
		return proA;
	}

	public void setProA(proveedor proA) {
		this.proA = proA;
	}

	public comunicacion getCom() {
		return com;
	}

	public void setCom(comunicacion com) {
		this.com = com;
	}

	public ubicacion getUbi() {
		return ubi;
	}

	public void setUbi(ubicacion ubi) {
		this.ubi = ubi;
	}

	public proveedorService getProService() {
		return proService;
	}

	public void setProService(proveedorService proService) {
		this.proService = proService;
	}

	public List<proveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<proveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}
	
	
}
