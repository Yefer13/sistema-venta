package jsf;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.servicios;
import services.serviciosService;

@ManagedBean(name="serviciosBean")
@ViewScoped
@SessionScoped
public class serviciosBean 
{
	private servicios serR;
	private servicios serA;
	private serviciosService serService = new serviciosService();
	private List<servicios> listaServicios;
	private int tamano_lista;
	
	public serviciosBean() throws Exception
	{
		serR = new servicios();
		serA = new servicios();
		lista();
	}
	
	public void registrar() throws Exception
	{
		if (serService.registrar(serR))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio " + serR.getNombre() + " Registrado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Servicio No Registrado",""));
		}
		serR = new servicios();
		lista();
	}
	
	public void actualizar() throws Exception
	{
		if (serService.actualizar(serA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio " + serA.getNombre() + " Actualizado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Servicio No Actualizado",""));
		}
		serA = new servicios();
		lista();
	}
	
	public void lista() throws Exception
	{
		listaServicios = serService.listar(serA);
		tamano_lista = listaServicios.size();
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay ningun Servicio / Registre uno",""));
			
		}
		else 
		{
			
		}
	}
	
	public void seleccionarServicio(servicios ser) throws Exception
	{
		this.serA = ser;
	}
	
	public List<servicios> listaBuscarSer(String se) throws Exception
	{
		return serService.listarS(se);
	}

	public servicios getSerR() {
		return serR;
	}

	public void setSerR(servicios serR) {
		this.serR = serR;
	}

	public servicios getSerA() {
		return serA;
	}

	public void setSerA(servicios serA) {
		this.serA = serA;
	}

	public serviciosService getSerService() {
		return serService;
	}

	public void setSerService(serviciosService serService) {
		this.serService = serService;
	}

	public List<servicios> getListaServicios() {
		return listaServicios;
	}

	public void setListaServicios(List<servicios> listaServicios) {
		this.listaServicios = listaServicios;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}
	
	
}
