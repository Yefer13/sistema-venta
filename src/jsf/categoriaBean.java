package jsf;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.categoria;
import services.categoriaService;

@ManagedBean(name="categoriaBean")
@SessionScoped
@ViewScoped
public class categoriaBean
{
	private categoria catR;
	private categoria catA;
	private categoriaService catService = new categoriaService();
	private List<categoria> listaCategoria;
	private int tamano_lista;
	private boolean accion;
	
	public categoriaBean() throws Exception
	{
		catR = new categoria();
		catA = new categoria();
		listaCategoria = new ArrayList<>();
		listar();
		
	}
	
	public void registrar() throws Exception
	{
		if (catService.registrar(catR))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria " + catR.getNombre() + " Registrado" ,""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Categoria No Registrado",""));
		}
		catR = new categoria();
		listar();
	}
	
	public void actualizar() throws Exception
	{
		if (catService.actualizar(catA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria " + catA.getNombre() + " Actualizado" ,""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Categoria No Actualizado",""));
		}
		
		catA = new categoria();
		listar();
	}
	
	public void listar() throws Exception
	{
		listaCategoria = catService.lista(catA);
		tamano_lista = listaCategoria.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO,"No hay Categorias / Registre Una Categoria",""));
			setAccion(false);
		}
		else 
		{
			setAccion(true);
		}
	}
	
	public void seleccionarCategoria(categoria cat) throws Exception
	{
		this.catA = cat;
	}
	
	public List<categoria> listabucarC(String c) throws Exception
	{
		return catService.listaC(c);
	}

	public categoria getCatR() {
		return catR;
	}

	public void setCatR(categoria catR) {
		this.catR = catR;
	}

	public categoria getCatA() {
		return catA;
	}

	public void setCatA(categoria catA) {
		this.catA = catA;
	}

	public categoriaService getCatService() {
		return catService;
	}

	public void setCatService(categoriaService catService) {
		this.catService = catService;
	}

	public List<categoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<categoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}
	
	
}
