package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.cliente;
import services.clienteService;
import util.FinalVariables;

@ManagedBean(name="clienteBean")
@SessionScoped
@ViewScoped
public class clienteBean 
{
	private cliente cliR;
	private cliente cliA;
	private clienteService cliService = new clienteService();
	private List<cliente> listaCliente;
	private List<cliente> listaClientePromo = new ArrayList<>();
	private int tamano_lista;
	private boolean accion;
	
	public clienteBean () throws Exception
	{
		cliR = new cliente();
		cliA = new cliente();
		listaCliente = new ArrayList<>();
		lista();
		listaPromocion();
	}
	
	public void registrar() throws Exception
	{
		cliR.setPromocion(new BigDecimal(0));
		
		try 
		{
			cliService.registrarPro(cliR);
			FacesContext.getCurrentInstance().addMessage(null, 
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente " + cliR.getNombres() + " " + cliR.getApellidoPaterno() + " " + cliR.getApellidoMaterno() + " Registrado",""));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			String ex = e.getCause().getCause().getCause().toString();
			if (ex.contains(FinalVariables.DUPLICATED)) 
			{
				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El Codigo " + cliR.getCodigo() + " Ya Existe / ESCRIBE OTRO CODIGO",""));
			}
			else 
			{
				FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cliente No Registrado",""));
			}
		}
		
		cliR = new cliente();
		lista();
	}
	
	public void actualizar() throws Exception
	{
		if (cliService.actualizar(cliA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "El Cliente " + cliR.getNombres() + " " + cliR.getApellidoPaterno() + " " + cliR.getApellidoMaterno() + " OBTUVO SU PROMOCION",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cliente No Actualizado",""));
		}
		cliA = new cliente();
		lista();
		listaPromocion();
	}
	
	public void actualizarP() throws Exception
	{
		try 
		{
			cliService.actualizarPro(cliA);
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente " + cliA.getNombres() + " Actualizado",""));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			String ex = e.getCause().getCause().getCause().toString();
			if (ex.contains(FinalVariables.DUPLICATED)) 
			{
				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "El Codigo " + cliR.getCodigo() + " Ya Existe / ESCRIBE OTRO CODIGO",""));
			}
			else 
			{
				FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cliente No Actualizado",""));
			}
		}
		cliA = new cliente();
		lista();
	}
	
	public void lista() throws Exception
	{
		listaCliente = cliService.listar(cliA);
		tamano_lista = listaCliente.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay ningun Cliente / Registre uno",""));
			setAccion(false);
		}
		else
		{
			setAccion(true);
		}
	}
	
	public void listaPromocion() throws Exception
	{
		listaClientePromo = cliService.listaPro();
		tamano_lista = listaClientePromo.size();
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No Hay Promocion",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "� PROMOCION DE AFINAMIENTO GRATIS !",""));
		}
	}
	
	public void seleccionarCliente(cliente clie) throws Exception
	{
		this.cliA = clie;
	}
	
	public List<cliente> listaBuscar(String c) throws Exception
	{
		return cliService.listaC(c);
	}

	public cliente getCliR() {
		return cliR;
	}

	public void setCliR(cliente cliR) {
		this.cliR = cliR;
	}

	public cliente getCliA() {
		return cliA;
	}

	public void setCliA(cliente cliA) {
		this.cliA = cliA;
	}

	public clienteService getCliService() {
		return cliService;
	}

	public void setCliService(clienteService cliService) {
		this.cliService = cliService;
	}

	public List<cliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}

	public List<cliente> getListaClientePromo() {
		return listaClientePromo;
	}

	public void setListaClientePromo(List<cliente> listaClientePromo) {
		this.listaClientePromo = listaClientePromo;
	}
	
	
}
