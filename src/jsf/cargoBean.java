package jsf;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.cargo;
import services.cargoService;

@ManagedBean(name="cargoBean")
@SessionScoped
@ViewScoped
public class cargoBean 
{
	private cargo carR;
	private cargo carA;
	private cargoService carService = new cargoService();
	private List<cargo> listaCargo;
	private int tamano_lista;
	private boolean accion;
	
	public cargoBean() throws Exception
	{
		carR = new cargo();
		carA = new cargo();
		listar();
	}
	
	public void registrar() throws Exception
	{
		if (carService.registrar(carR))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Cargo " + carR.getNombre() + " Registrado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Cargo No Registrado",""));
		}
		
		carR = new cargo();
		listar();
	}
	
	public void actualizar() throws Exception
	{
		if (carService.actualizar(carA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Cargo " + carA.getNombre() + " Actualizado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Cargo No Actualizado",""));
		}
		
		carA = new cargo();
		listar();
	}
	
	public void seleccionarCargo(cargo car) throws Exception
	{
		this.carA = car;
	}
	
	public void listar() throws Exception
	{
		listaCargo = carService.lista(carA);
		tamano_lista = listaCargo.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay ningun Cargo / Registre uno",""));
			setAccion(false);
		}
		else 
		{
			setAccion(true);
		}
	}
	
	public List<cargo> listaBuscarCar(String c) throws Exception
	{
		return carService.listaC(c);
	}

	public cargo getCarR() {
		return carR;
	}

	public void setCarR(cargo carR) {
		this.carR = carR;
	}

	public cargo getCarA() {
		return carA;
	}

	public void setCarA(cargo carA) {
		this.carA = carA;
	}

	public cargoService getCarService() {
		return carService;
	}

	public void setCarService(cargoService carService) {
		this.carService = carService;
	}

	public List<cargo> getListaCargo() {
		return listaCargo;
	}

	public void setListaCargo(List<cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}
	
	
}
