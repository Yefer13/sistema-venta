package jsf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import modelo.producto;
import util.Service;

@ManagedBean(name="metodoImagenBean")
@ApplicationScoped

public class metodoImagenBean 
{
	private producto pro;
	public metodoImagenBean()
	{
		pro = new producto();
	}
	public StreamedContent getImage() throws  IOException, SQLException
	{
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE)
		{
			return new DefaultStreamedContent();
		}
		else
		{
			String id = context.getExternalContext().getRequestParameterMap().get("pid");
			byte[] image = new Service().getProductImage(id);
			return new DefaultStreamedContent(new ByteArrayInputStream(image));
		}
	}
}
