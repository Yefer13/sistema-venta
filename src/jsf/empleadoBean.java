package jsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.cargo;
import modelo.comunicacion;
import modelo.datos;
import modelo.empleado;
import modelo.ubicacion;
import services.empleadoService;

@ManagedBean(name="empleadoBean")
@SessionScoped
@ViewScoped
public class empleadoBean 
{
	private empleado empR;
	private empleado empA;
	private datos dat;
	private comunicacion com;
	private ubicacion ubi;
	private cargo car;
	private empleadoService empService = new empleadoService();
	private List<empleado> listaEmpleado;
	private int tamano_lista;
	private boolean accion;
	
	public empleadoBean () throws Exception
	{
		empR = new empleado();
		empA = new empleado();
		dat = new datos();
		com = new comunicacion();
		ubi = new ubicacion();
		car = new cargo();
		listar();
	}
	
	public void registrar() throws Exception
	{
		empR.setTblCargo(car);
		car.setTblEmpleado(new ArrayList<empleado>(Arrays.asList(empR)));
		
		empR.setTblComunicacion(com);
		com.setTblEmpleado(empR);
		
		empR.setTblDatos(dat);
		dat.setTblEmpleado(empR);
		
		empR.setTblUbicacion(ubi);
		ubi.setTblEmpleado(empR);
		
		empR.setFechaRegistro(new Date());
		
		if (empService.registrar(empR))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Empleado " + empR.getTblDatos().getNombres() + " Registrado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empleado No Registrado",""));
		}
		
		empR = new empleado();
		dat = new datos();
		com = new comunicacion();
		ubi = new ubicacion();
		car = new cargo();
		listar();
		
	}
	
	public void Actualizar() throws Exception
	{
		if (empService.actualizar(empA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Empleado " + empA.getTblDatos().getNombres() + " Actualizado",""));
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empleado No Actualizado",""));
		}
		
		empA = new empleado();
		dat = new datos();
		com = new comunicacion();
		ubi = new ubicacion();
		car = new cargo();
		listar();
	}
	
	public void listar() throws Exception
	{
		listaEmpleado = empService.litar(empA);
		tamano_lista = listaEmpleado.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay ningun Empleado / Registre uno",""));
			setAccion(false);
		}
		else 
		{
			setAccion(true);
		}
	}
	
	public void seleccionarEmpleado(empleado empl) throws Exception
	{
		this.empA = empl ;
	}
	
	public List<empleado> listaBuscarE(String em) throws Exception
	{
		return empService.listaE(em);
	}
	
	public Date getMinAge()
	{
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.YEAR, -18);
		return currentDate.getTime();
		
	}
	
	public Date getMaxAge() 
	{
	    Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.YEAR, -65);
        
        return currentDate.getTime();
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}

	public empleado getEmpR() {
		return empR;
	}

	public void setEmpR(empleado empR) {
		this.empR = empR;
	}

	public empleado getEmpA() {
		return empA;
	}

	public void setEmpA(empleado empA) {
		this.empA = empA;
	}

	public datos getDat() {
		return dat;
	}

	public void setDat(datos dat) {
		this.dat = dat;
	}

	public comunicacion getCom() {
		return com;
	}

	public void setCom(comunicacion com) {
		this.com = com;
	}

	public ubicacion getUbi() {
		return ubi;
	}

	public void setUbi(ubicacion ubi) {
		this.ubi = ubi;
	}

	public cargo getCar() {
		return car;
	}

	public void setCar(cargo car) {
		this.car = car;
	}

	public empleadoService getEmpService() {
		return empService;
	}

	public void setEmpService(empleadoService empService) {
		this.empService = empService;
	}

	public List<empleado> getListaEmpleado() {
		return listaEmpleado;
	}

	public void setListaEmpleado(List<empleado> listaEmpleado) {
		this.listaEmpleado = listaEmpleado;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}
	
	

}
