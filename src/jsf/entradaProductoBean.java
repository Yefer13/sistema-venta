package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.detalleEntradaProducto;
import modelo.empleado;
import modelo.entradaProducto;
import modelo.producto;
import modelo.usuario;
import services.entradaProductoService;
import services.productoService;

@ManagedBean(name="entradaProductoBean")
@ViewScoped
@SessionScoped
public class entradaProductoBean 
{
	private entradaProducto entPro = new entradaProducto();
	private entradaProducto entProA = new entradaProducto();
	private detalleEntradaProducto detEntPro = new detalleEntradaProducto();
	private productoService proService = new productoService();
	private empleado emp = new empleado();
	private producto pro = new producto();
	private entradaProductoService entProService = new entradaProductoService();
	private List<producto> listaProducto = new ArrayList<>();
	private List<entradaProducto> listaentPro = new ArrayList<>();
	private List<detalleEntradaProducto> listaDetEntPro = new ArrayList<>();
	private List<detalleEntradaProducto> listaDetEntProSelec = new ArrayList<>();
	public entradaProductoBean () throws Exception
	{
		listarEntradProd();
		listarProducto();
	}
	
	public void agregar() 
	{		
		detEntPro.setTblEntradaProducto(entPro);
		if (listaDetEntPro.isEmpty())
		{
			if (detEntPro.getCantidad().intValueExact() == 0)
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe especificar la cantidad mayor que Cero",""));
			} 
			else 
			{
				listaDetEntPro.add(detEntPro);
			}
		}
		else
		{
			boolean exite = false;
			int index =0;
			for (int i = 0; i < listaDetEntPro.size(); i++) 
			{
				if (detEntPro.getTblProducto().getCodigoProducto().equals(listaDetEntPro.get(i).getTblProducto().getCodigoProducto()))
				{
					exite = true;
					index = i;
				}
			}
			if(exite)
			{
				if (detEntPro.getCantidad().intValueExact()==0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe especificar la cantidad mayor que Cero",""));
				}
				else
				{
					listaDetEntPro.get(index).setCantidad(listaDetEntPro.get(index).getCantidad().add(detEntPro.getCantidad()));
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"El producto ya existe, Se agregaron " + detEntPro.getCantidad() + "Unidades", ""));
				}
			}
			else
			{
				if (detEntPro.getCantidad().intValueExact() == 0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe especificar la cantidad mayor que Cero",""));
				}
				else
				{
					listaDetEntPro.add(detEntPro);
				}
			}
		}
		
		detEntPro = new detalleEntradaProducto();
	}
	
	public void quitar(detalleEntradaProducto de)
	{
		listaDetEntPro.remove(de);
	}
	
	public void registrar() throws Exception
	{
		entPro.setTblDetalleEntradaProducto(listaDetEntPro);
		entPro.setFechaRegistro(new Date());
		
		FacesContext context = FacesContext.getCurrentInstance();
		int idEmpleado = (int) context.getExternalContext().getSessionMap().get("idEmpleado");
		emp.setIdEmpleado(idEmpleado);
		entPro.setTblEmpleado(emp);
		
		if (listaDetEntPro.isEmpty())
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Agregue Productos a la Tabla",""));	
		} 
		else
		{
			if (entProService.registrar(entPro))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Entrada del Producto Registrado",""));	
				
				for(detalleEntradaProducto detEntPro : listaDetEntPro)
				{
					detEntPro.getTblProducto().setStock(detEntPro.getTblProducto().getStock().add(detEntPro.getCantidad()));
					proService.actualizarS(detEntPro.getTblProducto());
				}
			} 
			else
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Entrada No Registrada",""));
			}
		}
		
		emp = new empleado();
		pro = new producto();
		entPro = new entradaProducto();
		detEntPro = new detalleEntradaProducto();
		listaDetEntPro = new ArrayList<>();
		listaDetEntProSelec = new ArrayList<>();
		listarEntradProd();
	}
	
	public void actualizar() throws Exception
	{
		if (entProService.actualizar(entProA))
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Entrada Registrado",""));	
			
			for(detalleEntradaProducto detEntPro : listaDetEntPro)
			{
				detEntPro.getTblProducto().setStock(detEntPro.getTblProducto().getStock().add(detEntPro.getCantidad()));
				proService.actualizarS(detEntPro.getTblProducto());
			}
		} 
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Entrda No Registrada",""));
		}
		emp = new empleado();
		pro = new producto();
		entPro = new entradaProducto();
		detEntPro = new detalleEntradaProducto();
		listaDetEntPro = new ArrayList<>();
		listaDetEntProSelec = new ArrayList<>();
		listarEntradProd();
	}
	
	public void seleccionarEntrada(entradaProducto ent) throws Exception
	{
		this.entProA= ent;
	}
	
	public void listarProducto() throws Exception
	{
		listaProducto = proService.lista(pro);
	}
	
	public void listarEntradProd() throws Exception
	{
		this.listaentPro = entProService.listar();
	}
	
	public void select(List<detalleEntradaProducto> de) throws Exception
	{
		this.listaDetEntProSelec = de;
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}
	
	public entradaProducto getEntPro() {
		return entPro;
	}

	public void setEntPro(entradaProducto entPro) {
		this.entPro = entPro;
	}

	public detalleEntradaProducto getDetEntPro() {
		return detEntPro;
	}

	public void setDetEntPro(detalleEntradaProducto detEntPro) {
		this.detEntPro = detEntPro;
	}

	public empleado getEmp() {
		return emp;
	}

	public void setEmp(empleado emp) {
		this.emp = emp;
	}

	public producto getPro() {
		return pro;
	}

	public void setPro(producto pro) {
		this.pro = pro;
	}

	public entradaProductoService getEntProService() {
		return entProService;
	}

	public void setEntProService(entradaProductoService entProService) {
		this.entProService = entProService;
	}

	public List<entradaProducto> getListaentPro() {
		return listaentPro;
	}

	public void setListaentPro(List<entradaProducto> listaentPro) {
		this.listaentPro = listaentPro;
	}

	public List<detalleEntradaProducto> getListaDetEntPro() {
		return listaDetEntPro;
	}

	public void setListaDetEntPro(List<detalleEntradaProducto> listaDetEntPro) {
		this.listaDetEntPro = listaDetEntPro;
	}

	public productoService getProService() {
		return proService;
	}

	public void setProService(productoService proService) {
		this.proService = proService;
	}

	public List<detalleEntradaProducto> getListaDetEntProSelec() {
		return listaDetEntProSelec;
	}

	public void setListaDetEntProSelec(List<detalleEntradaProducto> listaDetEntProSelec) {
		this.listaDetEntProSelec = listaDetEntProSelec;
	}

	public List<producto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<producto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public entradaProducto getEntProA() {
		return entProA;
	}

	public void setEntProA(entradaProducto entProA) {
		this.entProA = entProA;
	}
	
	

}
