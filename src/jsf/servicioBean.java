package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import modelo.cliente;
import modelo.detalleServicio;
import modelo.empleado;
import modelo.servicio;
import modelo.servicios;
import modelo.venta;
import services.clienteService;
import services.servicioService;

@ManagedBean(name="servicioBean")
@SessionScoped
@ViewScoped
public class servicioBean
{
	private servicio ser;
	private servicio serA;
	private servicios sers;
	private cliente cli;
	private empleado emp;
	private detalleServicio detSer;
	private detalleServicio detSerE;
	private servicioService serService = new servicioService();
	private clienteService cliService = new clienteService();
	private List<servicio> listaServicio = new ArrayList<>();
	private List<servicio> listaSer;
	private List<detalleServicio> listaDetSer = new ArrayList<>();
	private List<detalleServicio> listaDetSerSelec;
	private List<detalleServicio> listaDS = new ArrayList<>();
	private int tamano_lista;
	
	private BigDecimal total;
	
	public servicioBean() throws Exception
	{
		ser = new servicio();
		serA= new servicio();
		sers = new servicios();
		cli = new cliente();
		emp = new empleado();
		detSer = new detalleServicio();
		detSerE = new detalleServicio();
		listarServicios();
	}
	
	public void agregar() 
	{
		detSer.setTblServicio(ser);
		
		if (listaDetSer.isEmpty())
		{
			if (detSer.getCantidad().intValueExact() == 0)
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad Mayor a 0",""));
			}
			else
			{
				listaDetSer.add(detSer);
				this.total = new BigDecimal(0);
				for(detalleServicio detServicio : listaDetSer)
				{
					BigDecimal precio = detServicio.getPrecioServicio();
					BigDecimal cantidad = detServicio.getCantidad();
					BigDecimal totall = precio.multiply(cantidad);
					total = total.add(totall);
				}
			}
		} 
		else
		{
			boolean existe = false;
			int index = 0;
			for (int i = 0; i < listaDetSer.size(); i++)
			{
				if (detSer.getTblServicios().getNombre().equals(listaDetSer.get(i).getTblServicios().getNombre()))
				{
					existe = true;
					index = i;
				}
			}
			
			if (existe)
			{
				if (detSer.getCantidad().intValueExact() == 0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad Mayor a 0",""));
				}
				else
				{
					listaDetSer.get(index).getTblServicios().getNombre().equals(detSer.getTblServicios().getNombre());
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"El producto ya existe " + detSer.getTblServicios().getNombre() + " en la Tabla", ""));
				}
			} 
			else
			{
				if (detSer.getCantidad().intValueExact() == 0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad Mayor a 0",""));
				}
				else
				{
					listaDetSer.add(detSer);
					this.total = new BigDecimal(0);
					for(detalleServicio detServicio : listaDetSer)
					{
						BigDecimal precio = detServicio.getPrecioServicio();
						BigDecimal cantidad = detServicio.getCantidad();
						BigDecimal totall = precio.multiply(cantidad);
						total = total.add(totall);
					}
				}
			}
		}
		detSer = new detalleServicio();
	}
	
	public void quitar(detalleServicio des)
	{
		listaDetSer.remove(des);
		
		this.total = new BigDecimal(0);
		for(detalleServicio detServicio : listaDetSer)
		{
			BigDecimal precio = detServicio.getPrecioServicio();
			BigDecimal cantidad = detServicio.getCantidad();
			BigDecimal totall = precio.multiply(cantidad);
			total = total.add(totall);
		}
	}
	
	public void registrar() throws Exception
	{
		ser.setTblDetalleServicio(listaDetSer);
		ser.setFechaRegistro(new Date());
		
		cli.setTblServicio(new ArrayList<servicio>(Arrays.asList(ser)));
		ser.setTblCliente(cli);
		
		ser.setCantidad(0);
	
		FacesContext context = FacesContext.getCurrentInstance();
		int idEmpleado = (int) context.getExternalContext().getSessionMap().get("idEmpleado");
		emp.setIdEmpleado(idEmpleado);
		ser.setTblEmpleado(emp);
		
		if (listaDetSer.isEmpty())
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Agregue Servicios a la tabla",""));	
		}
		else
		{
			if (serService.registrar(ser))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Servicio Registrado",""));	
				
				for(detalleServicio serLis : listaDetSer)
				{
					serLis.getTblServicio().getTblCliente().setPromocion(serLis.getTblServicio().getTblCliente().getPromocion().add(new BigDecimal(serLis.getCantidadP())));
					cliService.actualizarPro(serLis.getTblServicio().getTblCliente());;
				}
				
				BigDecimal monto = new BigDecimal(0);
				for(detalleServicio detSer : listaDetSer)
				{
					BigDecimal precio = detSer.getPrecioServicio();
					BigDecimal cantidad = detSer.getCantidad();
					BigDecimal total = precio.multiply(cantidad);
					monto = monto.add(total);
					System.err.println("el total es " + monto + "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					
					detSer.getTblServicio().setTotal(monto);
					serService.actualizar(detSer.getTblServicio());
				}
			} 
			else 
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Venta No Registrada",""));
			}
		}
		
		
		ser = new servicio();
		sers = new servicios();
		cli = new cliente();
		emp = new empleado();
		detSer = new detalleServicio();
		listaDetSer = new ArrayList<>();
		listarServicios();
		
	}
	
	public void eliminar() throws Exception
	{		
		if (ser.getTblDetalleServicio().iterator().next().getCantidadP() == 1 && ser.getTblCliente().getPromocion().doubleValue() >= 1)
		{
			BigDecimal resultado = new BigDecimal(0);
			double a = 1;
			BigDecimal promocion = ser.getTblCliente().getPromocion();
			BigDecimal resta = promocion.subtract(new BigDecimal(a));
			resultado = resultado.add(resta);
			System.err.println("La resta es" + resultado + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			ser.getTblCliente().setPromocion(resultado);
			cliService.actualizarPro(ser.getTblCliente());
			serService.eliminar(ser);
			
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_WARN," Servicio Elimado de la Fecha " + ser.getFechaRegistro() + "",""));
			RequestContext.getCurrentInstance().update("m");
			
		} 
		else
		{
			if (serService.eliminar(ser))
			{								
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Servicio Elimado de la Fecha "+ ser.getFechaRegistro() + "",""));
				RequestContext.getCurrentInstance().update("m");
			} 
			else
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Servicio No Elimado",""));
				RequestContext.getCurrentInstance().update("m");
			}
		}
		detSer = new detalleServicio();
		ser = new servicio();
		listarServicios();
	}
	
	public void listarServicios() throws Exception
	{
		this.listaServicio = serService.lista(ser);
	}
	
	public void selec(List<detalleServicio> des)
	{
		listaDetSerSelec = des;
	}
	
	@SuppressWarnings("unchecked")
	public void selecDeta(detalleServicio dee)
	{
		listaDetSerSelec = (List<detalleServicio>) dee;
	}
	
	public void seleccionarCliente(servicio de)
	{
		ser =  de;
	}
	
	public void seleccionarCliente2(servicio se)
	{
		serA  = se;
		
	}
	
	///  para listar los servicios ////
	
	public void listarCS() throws Exception
	{
		cliente cl = new cliente();
		cl.setNombres(cli.getNombres());
		listaSer = serService.listaCS(cl);
		tamano_lista = listaSer.size();
		if (tamano_lista <=0)
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"El Cliente " + cli.getNombres() + " " + cli.getApellidoPaterno() + " " + cli.getApellidoMaterno() + " no hizo ningun Servicio",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Cliente " + cli.getNombres() + " " + cli.getApellidoPaterno() + " " + cli.getApellidoMaterno() + " hizo estos Servicios" ,""));
		}
	}
	
	public void seleccionar() throws Exception
	{
		if (serA.getTblDetalleServicio().iterator().next().getCantidadP() == 1) 
		{
			///Queda este codigo
			BigDecimal resultado = new BigDecimal(0);
			double a = 1;
			BigDecimal promocion = serA.getTblCliente().getPromocion();
			BigDecimal Resta = promocion.subtract(new BigDecimal(a));
			resultado = resultado.add(Resta);
			System.err.println("La Resta es" + resultado + "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			serA.getTblCliente().setPromocion(resultado);
			cliService.actualizarPro(serA.getTblCliente());
		}
		else 
		{

		}
	}
	
	public void actualizarClienteServicio() throws Exception
	{
		
		if (serA.getTblDetalleServicio().iterator().next().getCantidadP() == 1)
		{	
			
			///Queda este codigo
			BigDecimal resultado = new BigDecimal(0);
			double a = 1;
			BigDecimal promocion = serA.getTblCliente().getPromocion();
			BigDecimal suma = promocion.add(new BigDecimal(a));
			resultado = resultado.add(suma);
			System.err.println("La Suma es" + resultado + "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			serA.getTblCliente().setPromocion(resultado);
			cliService.actualizarPro(serA.getTblCliente());
			serService.actualizarS(serA);
			
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO," Se Actualizo el Cliente de la Fecha " + serA.getFechaRegistro() + "",""));
			RequestContext.getCurrentInstance().update("m");
		} 
		else
		{
						
			if (serService.actualizarS(serA))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO," Se Actualizo el Cliente de la Fecha " + serA.getFechaRegistro() + "",""));
				RequestContext.getCurrentInstance().update("m");
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cliente No Actualizado",""));
				RequestContext.getCurrentInstance().update("m");
			}
		}
		
		serA = new servicio();
		cli = new cliente();
		detSer = new detalleServicio();
		listarServicios();
		
	}
	
	////DETALLE SERVICIOS//////
	
	public void seleccionarDetalle(detalleServicio d) 
	{
		detSer = d;
	}
	
	public void seleccionarZ(List<detalleServicio> m) throws Exception
	{
		listaDS = m;
	}
	
	public void eliminarDetalle(detalleServicio d) throws Exception
	{
//		if ()
//		{
			
				serService.eliminarDD(d);
			
			
			System.err.println("ARRCHIVO " + detSer + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			if (detSer.getCantidadP() == 1) 
//			{
//				
//			BigDecimal b = new BigDecimal(0);
//			double c = 1;
//			BigDecimal promo = detSer.getTblServicio().getTblCliente().getPromocion();
//			System.err.println("PROMOCION ES " + promo + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			BigDecimal resta = promo.subtract(new BigDecimal(c));
//			b = b.add(resta);
//			System.err.println("RESTA ES " + b + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			detSer.getTblServicio().getTblCliente().setPromocion(b);
//			cliService.actualizarPro(detSer.getTblServicio().getTblCliente());
//			
//			BigDecimal a = new BigDecimal(0);
//			BigDecimal precio = detSer.getPrecioServicio();
//			System.err.println("PRECIO ES " + precio + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			BigDecimal total = detSer.getTblServicio().getTotal();
//			System.err.println("TOTAL ES " + total + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			BigDecimal resultado = total.subtract(precio);
//			BigDecimal aa = a.add(resultado);
//			System.err.println("RESULTADO ES " + aa + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			detSer.getTblServicio().setTotal(aa);
//			serService.actualizar(detSer.getTblServicio());
//			
//			}
			
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Detalle  Eliminado",""));
			RequestContext.getCurrentInstance().update("ppm");
//		} 
//		else
//		{
//			if (detSer.getCantidadP() == 0) 
//			{
//				BigDecimal a = new BigDecimal(0);
//				BigDecimal precio = detSer.getPrecioServicio();
//				System.err.println("PRECIO ES " + precio + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//				BigDecimal total = detSer.getTblServicio().getTotal();
//				System.err.println("TOTAL ES " + total + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//				BigDecimal resultado = total.subtract(precio);
//				BigDecimal aa = a.add(resultado);
//				System.err.println("RESULTADO ES " + aa + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//				detSer.getTblServicio().setTotal(aa);
//				serService.actualizar(detSer.getTblServicio());
//				
//				serService.eliminarD(detSer);
//			} 
//			else
//			{
//				
//			}
//			FacesContext.getCurrentInstance().addMessage
//			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Detalle No Eliminado",""));
//			RequestContext.getCurrentInstance().update("ppm");
//		}
		
		detSer = new detalleServicio();
		listarServicios();
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}

	public servicio getSer() {
		return ser;
	}

	public void setSer(servicio ser) {
		this.ser = ser;
	}

	public servicios getSers() {
		return sers;
	}

	public void setSers(servicios sers) {
		this.sers = sers;
	}

	public cliente getCli() {
		return cli;
	}

	public void setCli(cliente cli) {
		this.cli = cli;
	}

	public empleado getEmp() {
		return emp;
	}

	public void setEmp(empleado emp) {
		this.emp = emp;
	}

	public detalleServicio getDetSer() {
		return detSer;
	}

	public void setDetSer(detalleServicio detSer) {
		this.detSer = detSer;
	}

	public servicioService getSerService() {
		return serService;
	}

	public void setSerService(servicioService serService) {
		this.serService = serService;
	}

	public List<servicio> getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List<servicio> listaServicio) {
		this.listaServicio = listaServicio;
	}

	public List<detalleServicio> getListaDetSer() {
		return listaDetSer;
	}

	public void setListaDetSer(List<detalleServicio> listaDetSer) {
		this.listaDetSer = listaDetSer;
	}

	public List<detalleServicio> getListaDetSerSelec() {
		return listaDetSerSelec;
	}

	public void setListaDetSerSelec(List<detalleServicio> listaDetSerSelec) {
		this.listaDetSerSelec = listaDetSerSelec;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public List<servicio> getListaSer() {
		return listaSer;
	}

	public void setListaSer(List<servicio> listaSer) {
		this.listaSer = listaSer;
	}

	public servicio getSerA() {
		return serA;
	}

	public void setSerA(servicio serA) {
		this.serA = serA;
	}

	public clienteService getCliService() {
		return cliService;
	}

	public void setCliService(clienteService cliService) {
		this.cliService = cliService;
	}

	public detalleServicio getDetSerE() {
		return detSerE;
	}

	public void setDetSerE(detalleServicio detSerE) {
		this.detSerE = detSerE;
	}

	public List<detalleServicio> getListaDS() {
		return listaDS;
	}

	public void setListaDS(List<detalleServicio> listaDS) {
		this.listaDS = listaDS;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
	
}
