package jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import modelo.usuario;
@ManagedBean(name="plantillaController")
@ViewScoped
public class plantillaController
{
	public void verificarSesion()
	{
		try 
		{
			FacesContext context = FacesContext.getCurrentInstance();
			usuario usurio = (usuario) context.getExternalContext().getSessionMap().get("usua");
			
			if(usurio == null)
			{
				context.getExternalContext().redirect("../Paginas/Login.xhtml");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void cerrarSesion()
	{
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}
}
