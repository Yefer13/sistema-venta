package jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import modelo.datos;
import modelo.empleado;
import modelo.usuario;
import services.empleadoService;
import services.usuarioServices;

@ManagedBean(name="usuarioBean")
@SessionScoped
@ViewScoped
public class usuarioBean 
{
	private String f = "Administrador", s="Almacenero", g="Vendedor";
	private usuario usu;
	private usuario usuu;
	private empleado empl;
	private datos dat;
	private usuarioServices ususervice = new usuarioServices();
	private empleadoService emplService = new empleadoService();
	private List<usuario> listarUsuario;
	private List<empleado> listarEmpleado = new ArrayList<>();
	private int tamano_lista;
	private boolean accion;
	
	public usuarioBean() throws Exception
	{
		usu = new usuario();
		usuu = new usuario();
		empl = new empleado();
		dat = new datos();
		listarEmpleado = new ArrayList<>();
		listarUsuario = new ArrayList<>();
		listar();
		listarEmpleados();
	}
	
	public void registrar() throws Exception
	{
		empl.setTblUsuario(usuu);
		usuu.setTblEmpleado(empl);
		if (ususervice.registrar(usuu))
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario " + usuu.getUsuario() + " Registrado", ""));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario No Registrado", ""));
		}
		listar();
		usu = new usuario();
		usuu = new usuario();
		empl = new empleado();
		listarEmpleados();
	}
	
	public void actualizar() throws Exception
	{
		if (ususervice.actualizar(usu))
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario " + usu.getUsuario() + " Actualizado", ""));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario No Actualizado", ""));
		}
		listar();
		usu = new usuario();
		usuu = new usuario();
		empl = new empleado();
		listarEmpleados();
	}
	
	public void seleccionarUsuario(usuario usuar) throws  Exception
	{
		usuar.setContrasena("");
		this.usu = usuar;
	}
	
	public void listar() throws Exception
	{
		listarUsuario = ususervice.lista(usu);
		tamano_lista = listarUsuario.size();
		
		if (tamano_lista <=0)
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay Usuarios Registre Uno", ""));
			setAccion(false);
		} else {
			setAccion(true);
		}
	}
	
	public String validarUsuario() throws Exception
	{
		String validar = null;
		usuario usurio = new usuario();
		try 
		{
			usurio = ususervice.validar(usu);
			Thread.sleep(1000);
			if( usurio.getUsuario()!=null)
			{
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usua",usurio);
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idEmpleado",usurio.getTblEmpleado().getIdEmpleado());
				validar = "/Paginas/Menu-Principal.xhtml?faces-redirect=true";
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Incorrecto", ""));
			}
		} 
		catch (Exception e)
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "No funka Prro", ""));
		}
		return validar;
	}
	
	public void listarEmpleados() throws Exception
	{
		listarEmpleado = emplService.listarPorCargos(f, s, g);
	}
	
	public void seleccionarEmpleado(empleado e) throws Exception
	{
		this.empl = e;
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}
	
	public usuario getUsu() {
		return usu;
	}

	public void setUsu(usuario usu) {
		this.usu = usu;
	}

	public usuario getUsuu() {
		return usuu;
	}

	public void setUsuu(usuario usuu) {
		this.usuu = usuu;
	}

	public usuarioServices getUsuservice() {
		return ususervice;
	}

	public void setUsuservice(usuarioServices ususervice) {
		this.ususervice = ususervice;
	}

	public List<usuario> getListarUsuario() {
		return listarUsuario;
	}

	public void setListarUsuario(List<usuario> listarUsuario) {
		this.listarUsuario = listarUsuario;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}

	public empleado getEmpl() {
		return empl;
	}

	public void setEmpl(empleado empl) {
		this.empl = empl;
	}

	public datos getDat() {
		return dat;
	}

	public void setDat(datos dat) {
		this.dat = dat;
	}

	public String getF() {
		return f;
	}

	public void setF(String f) {
		this.f = f;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getG() {
		return g;
	}

	public void setG(String g) {
		this.g = g;
	}

	public empleadoService getEmplService() {
		return emplService;
	}

	public void setEmplService(empleadoService emplService) {
		this.emplService = emplService;
	}

	public List<empleado> getListarEmpleado() {
		return listarEmpleado;
	}

	public void setListarEmpleado(List<empleado> listarEmpleado) {
		this.listarEmpleado = listarEmpleado;
	}
	
	
}
