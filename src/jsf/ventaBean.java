package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import daoImplement.ventaDaoImpl;
import modelo.cliente;
import modelo.detalleVenta;
import modelo.empleado;
import modelo.producto;
import modelo.venta;
import services.productoService;
import services.ventaService;

@ManagedBean(name="ventaBean")
@SessionScoped
@ViewScoped
public class ventaBean 
{
	private venta ven;
	private venta venA;
	private producto pro;
	private cliente cli;
	private empleado emp;
	private detalleVenta detVen;
	private ventaService venService = new ventaService();
	private productoService proService = new productoService();
	private List<venta> listaVenta = new ArrayList<>();
	private List<venta> listaVen;
	private List<venta> listaVentaN;
	private List<detalleVenta> listaDetVen = new ArrayList<>();
	private List<detalleVenta> listaDetVenSelec;
	private int tamano_lista;
	private BigDecimal cantidaD;
	private BigDecimal total;
	detalleVenta aa = new detalleVenta();
	
	private BigDecimal numeroVenta;
	
	public ventaBean() throws Exception
	{
		ven = new venta();
		venA = new venta();
		pro = new producto();
		cli = new cliente();
		emp = new empleado();
		detVen = new detalleVenta();
		listarVenta();
	}
	
	///numero para generar en las ventas///
	
//	public void numeroVenta() throws Exception
//	{
//		this.numeroVenta = (BigDecimal) venService.listaTotalRegistro();
//		
//		if (this.numeroVenta.doubleValue() <= 0 || this.numeroVenta==null) 
//		{
//			BigDecimal aa = new BigDecimal(1);
//			this.numeroVenta.add(aa);
//		}
//		else 
//		{
//			BigDecimal cc = new BigDecimal(1);
//			ven = (venta) venService.listaUltimoRegistro();
//			this.numeroVenta = this.ven.getNumeroVenta().add(cc);
//		}
//	}
	
	///NO FUNKA FALTA ARREGLAR
	public void numeroVentaa() throws Exception
	{
		if (ven.getNumeroVenta().doubleValue() <= 0 || ven.getNumeroVenta().equals(null) || ven.getNumeroVenta().doubleValue() >0)
		{
			BigDecimal a = new BigDecimal(0);
			for (venta ve : listaVentaN) 
			{
				double z = 1;
				BigDecimal nv = ve.getNumeroVenta();
				BigDecimal rs = nv.add(new BigDecimal(z));
				a = a.add(rs);
				System.err.println("el total es " + rs + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			}
		} 
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"NO DIO PRRO",""));
		}
		
	}
	
	////////////////////////////////////////////
	
	public void actualizarFila(RowEditEvent event) throws Exception
	{	
		
		detalleVenta det = (detalleVenta) event.getObject();
//		detVen.setCantidad(det.getCantidad());
		
		det.setCantidad(cantidaD);
		
//		venService.actualizarDet(det);
		
		
		System.err.println("cantidad que pega " + getCantidaD() + "---------" + cantidaD + " +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		//// TODO ME JALA NULO U_U 
		
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"se actualizo",""));
		

		
	}
	
	public void cancelar(RowEditEvent event)
	{
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"se cancelo",""));
	}
	
//	public BigDecimal getcantidad()
//	{
//		BigDecimal canti = new BigDecimal(0);
//		for (int i = 0; i < listaDetVen.size(); i++)
//		{
//			canti = canti.add(listaDetVen.get(i).getCantidad());
//		}
//		return canti;
//	}
	
	public void calcularTotal() 
	{
		this.total = new BigDecimal(0);
		for(detalleVenta venDet : listaDetVen)
		{
			BigDecimal precio = venDet.getTblProducto().getPrecioVenta();
			BigDecimal cantidad = venDet.getCantidad();
			BigDecimal totall = precio.multiply(cantidad);
			total = total.add(totall);
		}
		
		listaDetVen = new ArrayList<>();
	}
	
	public void agregar()
	{
		detVen.setTblVenta(ven);
		
		if (listaDetVen.isEmpty())
		{
			if (detVen.getCantidad().intValueExact() == 0)
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
			}
			else
			{
				if (detVen.getCantidad().doubleValue() > Double.parseDouble(detVen.getTblProducto().getStock()+""))
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+ detVen.getTblProducto().getNombre()+" solo se cuenta con "+ detVen.getTblProducto().getStock() +"unidades",""));
				} 
				else
				{
					listaDetVen.add(detVen);
					this.total = new BigDecimal(0);
					for(detalleVenta venDet : listaDetVen)
					{
						BigDecimal precio = venDet.getTblProducto().getPrecioVenta();
						BigDecimal cantidad = venDet.getCantidad();
						BigDecimal totall = precio.multiply(cantidad);
						total = total.add(totall);
					}
				}
			}
		}
		else
		{
			boolean existe = false;
			int index= 0;
			for (int i = 0; i < listaDetVen.size(); i++)
			{
				if (detVen.getTblProducto().getCodigoProducto().equals(listaDetVen.get(i).getTblProducto().getCodigoProducto())) 
				{
					existe = true;
					index = i;
				}
			}
			
			if (existe)
			{
				if (detVen.getCantidad().intValueExact() == 0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				} 
				else
				{
					if ((listaDetVen.get(index).getCantidad().add(detVen.getCantidad())).doubleValue()> Double.parseDouble(listaDetVen.get(index).getTblProducto().getStock()+"")) 
					{
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+ detVen.getTblProducto().getNombre()+" solo se cuenta con "+ detVen.getTblProducto().getStock() +"unidades",""));
					} 
					else 
					{
						listaDetVen.get(index).setCantidad(listaDetVen.get(index).getCantidad().add(detVen.getCantidad()));
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"El producto ya existe, Se agregaron " + detVen.getCantidad() + "Unidades", ""));
						
						this.total = new BigDecimal(0);
						for(detalleVenta venDet : listaDetVen)
						{
							BigDecimal precio = venDet.getTblProducto().getPrecioVenta();
							BigDecimal cantidad = venDet.getCantidad();
							BigDecimal totall = precio.multiply(cantidad);
							total = total.add(totall);
						}
					}
				}
			}
			else 
			{
				if (detVen.getCantidad().intValueExact() == 0) 
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				}
				else 
				{
					if (detVen.getCantidad().doubleValue() > Double.parseDouble(detVen.getTblProducto().getStock() + ""))
					{
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+ detVen.getTblProducto().getNombre()+" solo se cuenta con "+ detVen.getTblProducto().getStock() +"unidades",""));
					}
					else
					{
						listaDetVen.add(detVen);
						
						this.total = new BigDecimal(0);
						for(detalleVenta venDet : listaDetVen)
						{
							BigDecimal precio = venDet.getTblProducto().getPrecioVenta();
							BigDecimal cantidad = venDet.getCantidad();
							BigDecimal totall = precio.multiply(cantidad);
							total = total.add(totall);
						}
					}
				}
			}
		}
		detVen = new detalleVenta();
		
	}
	
	public void quitar(detalleVenta dev)
	{
		listaDetVen.remove(dev);
		
		this.total = new BigDecimal(0);
		for(detalleVenta venDet : listaDetVen)
		{
			BigDecimal precio = venDet.getTblProducto().getPrecioVenta();
			BigDecimal cantidad = venDet.getCantidad();
			BigDecimal totall = precio.multiply(cantidad);
			total = total.add(totall);
		}
	}
	
	public void registrar() throws Exception
	{
		ven.setTblDetalleVenta(listaDetVen);
		ven.setFechaRegistro(new Date());
		
		cli.setTblVenta(new ArrayList<venta>(Arrays.asList(ven)));
		ven.setTblCliente(cli);
		
		FacesContext context = FacesContext.getCurrentInstance();
		int idEmpleado = (int) context.getExternalContext().getSessionMap().get("idEmpleado");
		emp.setIdEmpleado(idEmpleado);
		ven.setTblEmpleado(emp);
		
		if (listaDetVen.isEmpty())
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Agregue Productos en la Tabla de Ventas",""));	
		}
		else
		{
			if (venService.registrar(ven))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Venta Registrado",""));	
				
				for(detalleVenta detVeen : listaDetVen)
				{
					detVeen.getTblProducto().setStock(detVeen.getTblProducto().getStock().subtract( detVeen.getCantidad()));
					proService.actualizarS(detVeen.getTblProducto());
					
				}		
				
				BigDecimal preciototal = new BigDecimal(0);
				for (int i = 0; i < this.listaDetVen.size(); i++)
				{
					BigDecimal precio = listaDetVen.get(i).getTblProducto().getPrecioVenta().multiply(listaDetVen.get(i).getCantidad());
//					BigDecimal cantidad = listaDetVen.get(i).getCantidad();
//					BigDecimal total = precio;
//					BigDecimal suma = preciototal.add(precio);
					preciototal = preciototal.add(precio);
					System.err.println("el total es " + preciototal + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					
				 	listaDetVen.get(i).getTblVenta().setTotal(preciototal);
					venService.actualizar(listaDetVen.get(i).getTblVenta());
//					preciototal.add(precio);
				}
				
				
//				 for(detalleVenta det : listaDetVen)
//					{	
//					 	BigDecimal precio = det.getTblProducto().getPrecioVenta();
//					 	BigDecimal cantidad = det.getCantidad();
//					 	BigDecimal total = precio.multiply(cantidad);
////					 	BigDecimal suma = total.add(preciototal);
//					 	System.err.println("el total es " + total + "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
////								monto.add(total);
////					 	ven.setTotal(preciototal);
//					 	det.getTblVenta().setTotal(preciototal.add(total));
////							 	total.add(monto);
////								ven.setTotal(monto);
//						venService.actualizar(det.getTblVenta());
//						 
//					}	
				
			} 
			else
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Venta No Registrada",""));
			}
		}
		
		ven = new venta();
		pro = new producto();
		cli = new cliente();
		emp = new empleado();
		detVen = new detalleVenta();
		listaDetVen = new ArrayList<>();
		listarVenta();
	}
	
	public void listarVenta() throws Exception
	{
		this.listaVenta = venService.lista();
	}
	
	public void selec(List<detalleVenta> dev)
	{
		listaDetVenSelec = dev;
	}
	
	public void seleccionarCliente(venta v)
	{
		this.venA = v;
	}
	
	///// LISTAR CLIENTE CON SU RESPECTIVO PRODUCTO///
	
	public void listarCP() throws Exception
	{
		cliente cl = new cliente();
		cl.setNombres(cli.getNombres());
		listaVen = venService.listaCP(cl);
		tamano_lista = listaVen.size();
		if (tamano_lista <=0)
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"El Cliente " + cli.getNombres() + " " + cli.getApellidoPaterno() + " " + cli.getApellidoMaterno() + " no hizo ninguna compra",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El Cliente " + cli.getNombres() + " " + cli.getApellidoPaterno() + " " + cli.getApellidoMaterno() + " hizo estas compras" ,""));
		}
	}
	
	public void actualizarClienteVenta() throws Exception
	{
		if (venService.actualizarV(venA))
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Cliente Actualizado",""));
		} 
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Cliente No Actualizado",""));
		}
		venA = new venta();
		listarVenta();
	}
	
	public void eliminarVenta() throws Exception
	{
		if (venService.eliminar(ven))
		{
			
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Venta Eliminado",""));	
			
			BigDecimal a = new BigDecimal(0);			
			for(detalleVenta aa : ven.getTblDetalleVenta())
			{
				BigDecimal cantidadd = aa.getCantidad();
				System.err.println("La cantidad es" + cantidadd + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				BigDecimal stockk = aa.getTblProducto().getStock();
				System.err.println("La stock es" + stockk + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				BigDecimal sumaa = cantidadd.add(stockk);
				BigDecimal aac = a.add(sumaa);
//				a = a.add(sumaa);
				System.err.println("La suma es" + aac + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				aa.getTblProducto().setStock(aac);
				proService.actualizarS(aa.getTblProducto());
			}
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Venta No Elimado",""));	
		}
		ven = new venta();
		listarVenta();
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}
	
	public venta getVen() {
		return ven;
	}

	public void setVen(venta ven) {
		this.ven = ven;
	}

	public producto getPro() {
		return pro;
	}

	public void setPro(producto pro) {
		this.pro = pro;
	}

	public cliente getCli() {
		return cli;
	}

	public void setCli(cliente cli) {
		this.cli = cli;
	}

	public empleado getEmp() {
		return emp;
	}

	public void setEmp(empleado emp) {
		this.emp = emp;
	}

	public detalleVenta getDetVen() {
		return detVen;
	}

	public void setDetVen(detalleVenta detVen) {
		this.detVen = detVen;
	}

	public ventaService getVenService() {
		return venService;
	}

	public void setVenService(ventaService venService) {
		this.venService = venService;
	}

	public productoService getProService() {
		return proService;
	}

	public void setProService(productoService proService) {
		this.proService = proService;
	}

	public List<venta> getListaVenta() {
		return listaVenta;
	}

	public void setListaVenta(List<venta> listaVenta) {
		this.listaVenta = listaVenta;
	}

	public List<detalleVenta> getListaDetVen() {
		return listaDetVen;
	}

	public void setListaDetVen(List<detalleVenta> listaDetVen) {
		this.listaDetVen = listaDetVen;
	}

	public List<detalleVenta> getListaDetVenSelec() {
		return listaDetVenSelec;
	}

	public void setListaDetVenSelec(List<detalleVenta> listaDetVenSelec) {
		this.listaDetVenSelec = listaDetVenSelec;
	}



	public List<venta> getListaVen() {
		return listaVen;
	}



	public void setListaVen(List<venta> listaVen) {
		this.listaVen = listaVen;
	}



	public int getTamano_lista() {
		return tamano_lista;
	}



	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public BigDecimal getCantidaD() {
		return cantidaD;
	}

	public void setCantidaD(BigDecimal cantidaD) {
		this.cantidaD = cantidaD;
	}

	public venta getVenA() {
		return venA;
	}

	public void setVenA(venta venA) {
		this.venA = venA;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getNumeroVenta() {
		return numeroVenta;
	}

	public void setNumeroVenta(BigDecimal numeroVenta) {
		this.numeroVenta = numeroVenta;
	}

	
		
}
