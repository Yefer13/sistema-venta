package jsf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;

import modelo.categoria;
import modelo.producto;
import modelo.proveedor;
import services.productoService;
import services.proveedorService;
import util.FinalVariables;
import util.UtilJsf;

@ManagedBean(name="productoBean")
@SessionScoped
@ViewScoped
public class productoBean
{
	private producto proR;
	private producto proA;
	private productoService proService = new productoService();
	private proveedorService provService = new proveedorService();
	private proveedor prov;
	private categoria cat;
	private String imagenProducto;
	private List<producto> listaProducto = new ArrayList<>();
	private List<producto> listaStock = new ArrayList<>();
	private List<proveedor> listaProveedor = new ArrayList<>();
	private List<proveedor> seleccionarProv = new ArrayList<>();
	private int tamano_lista;
	private boolean accion;
	
	public productoBean()throws Exception
	{
		proR = new producto();
		proA = new producto();
		prov = new proveedor();
		cat = new categoria();
		listar();
		listarStock();
	}
	
	public void registrar() throws Exception
	{
		proR.setTblCategoria(cat);
		cat.setTblProducto(new ArrayList<producto>(Arrays.asList(proR)));
		
		proR.setTblProveedor(listaProveedor);
		proR.setFechaRegistro(new Date());
		proR.setStock(new BigDecimal(0));
		
		try 
		{
			proService.registrar(proR);
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO,"Producto " + proR.getNombre() + " Registrado",""));
		} catch (Exception e) 
		{
			e.printStackTrace();
			String ex = e.getCause().getCause().getCause().toString();
			if (ex.contains(FinalVariables.DUPLICATED)) 
			{
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Codigo " + proR.getCodigoProducto() + " del Producto Ya Existe! / Escribe otro Codigo", ""));
			}
			else 
			{
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"No se pudo registrar el Producto", null));
			}
		}
		
		proR = new producto();
		cat = new categoria();
		listaProveedor = new ArrayList<>();
		listar();
		listarStock();
	}
	
	public void actulizarProducto() throws Exception
	{
		if (proService.actualizar(proA))
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Proveedor " + proA.getNombre() + " Actualizado",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Proveedor " + proA.getNombre() + " No Actualizado",""));
		}
		
		proA = new producto();
		cat = new categoria();
		prov = new proveedor();
		listar();
	}
	
	public void actualizarProductoo() throws Exception
	{
		try
		{
			proService.actualizarPro(proA);
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO,"Producto " + proA.getNombre()  + " Actualizado",""));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String ex = e.getCause().getCause().getCause().toString();
			if (ex.contains(FinalVariables.DUPLICATED)) 
			{
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Codigo " + proA.getCodigoProducto() + " del Producto Ya Existe! / ESCRIBE OTRO CODIGO" , ""));
			}
			else 
			{
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"No se pudo Actualizar el Producto", null));
			}
		}
		
		proA = new producto();
		cat = new categoria();
		prov = new proveedor();
		listar();
	}
	
	public void seleccionarProducto(producto produ) throws Exception
	{
		this.proA = produ;
	}
	
	public void listar() throws Exception
	{
		listaProducto = proService.lista(proR);
		tamano_lista = listaProducto.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO,"Registre un Producto",""));
			setAccion(false);
		}
		else 
		{
			setAccion(true);
		}
		
		
	}
	
	public void listarStock() throws Exception
	{
		listaStock = proService.listaPS();
		tamano_lista = listaStock.size();
		
		if (tamano_lista <= 0)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO,"Productos Estables",""));
			
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_WARN,"Existen Productos que se estan Agotando / Revice el Stock",""));
		}
	}
	
	public void seleccionarProvedor(List<proveedor> prov)
	{
		this.seleccionarProv = prov;
	}
	
	public List<proveedor> listaBuscar(String p) throws Exception
	{
		return provService.listaP(p);
	}
	
	public List<producto> listaBuscarP(String p) throws Exception
	{
		return proService.listarP(p);
	}
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}
	
	
	////Para Subir Imagen 
	
	public void subirImagen(FileUploadEvent event)
	{
		try 
		{
			proR.setImg(event.getFile().getContents());
			imagenProducto = UtilJsf.guardarBlobEnFicheroTemporal(proR.getImg(), event.getFile().getFileName());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Imagen Preparado para el Registro", ""));
		}
		catch (Exception e) 
		{

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registro No Exitoso", ""));
		}
	}

	public producto getProR() {
		return proR;
	}

	public void setProR(producto proR) {
		this.proR = proR;
	}

	public producto getProA() {
		return proA;
	}

	public void setProA(producto proA) {
		this.proA = proA;
	}

	public productoService getProService() {
		return proService;
	}

	public void setProService(productoService proService) {
		this.proService = proService;
	}

	public proveedor getProv() {
		return prov;
	}

	public void setProv(proveedor prov) {
		this.prov = prov;
	}

	public categoria getCat() {
		return cat;
	}

	public void setCat(categoria cat) {
		this.cat = cat;
	}

	public List<proveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<proveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public List<producto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<producto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public int getTamano_lista() {
		return tamano_lista;
	}

	public void setTamano_lista(int tamano_lista) {
		this.tamano_lista = tamano_lista;
	}

	public boolean isAccion() {
		return accion;
	}

	public void setAccion(boolean accion) {
		this.accion = accion;
	}

	public proveedorService getProvService() {
		return provService;
	}

	public void setProvService(proveedorService provService) {
		this.provService = provService;
	}

	public List<proveedor> getSeleccionarProv() {
		return seleccionarProv;
	}

	public void setSeleccionarProv(List<proveedor> seleccionarProv) {
		this.seleccionarProv = seleccionarProv;
	}

	public List<producto> getListaStock() {
		return listaStock;
	}

	public void setListaStock(List<producto> listaStock) {
		this.listaStock = listaStock;
	}

	public String getImagenProducto() {
		return imagenProducto;
	}

	public void setImagenProducto(String imagenProducto) {
		this.imagenProducto = imagenProducto;
	}
	
	
}
