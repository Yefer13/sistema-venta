package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="usuario")
public class usuario 
{
	@Id
	@GeneratedValue
	private int idUsuario;

	private String usuario;
	
	@Size(min = 5 , max=20, message = "La contraseņa tiene que ser mayor de 5 y menor de 20 digitos")
	private String contrasena;
	
	@OneToOne
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}
	
	
}
