package modelo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detalleVenta")
public class detalleVenta 
{
	@Id
	@GeneratedValue
	private int idDetalleVenta;
	private BigDecimal cantidad;
	
	@ManyToOne
	@JoinColumn(name="idVenta")
	private venta tblVenta;
	
	@ManyToOne
	@JoinColumn(name="idProducto")
	private producto tblProducto;

	public int getIdDetalleVenta() {
		return idDetalleVenta;
	}

	public void setIdDetalleVenta(int idDetalleVenta) {
		this.idDetalleVenta = idDetalleVenta;
	}
	

	

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public venta getTblVenta() {
		return tblVenta;
	}

	public void setTblVenta(venta tblVenta) {
		this.tblVenta = tblVenta;
	}

	public producto getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(producto tblProducto) {
		this.tblProducto = tblProducto;
	}
	
	
}
