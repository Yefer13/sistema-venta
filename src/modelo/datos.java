package modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="datos")
public class datos 
{
	@Id
	@GeneratedValue
	private int idDatos;
	
	@NotEmpty(message= "Escribir un Nombre")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Nombre solo permite letras")
	private String nombres;
	
	@NotEmpty(message = "Escribir el Apellido Paterno")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Apellido Paterno solo permite letras")
	private String apellidoPaterno;
	
	@NotEmpty(message = "Escribir el Apellido Materno")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Apellido Materno solo permite letras")
	private String apellidoMaterno;
	
	@NotNull(message = "Fecha de Nacimiento Necesario")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@OneToOne(mappedBy="tblDatos")
	private empleado tblEmpleado;

	public int getIdDatos() {
		return idDatos;
	}

	public void setIdDatos(int idDatos) {
		this.idDatos = idDatos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}
	
	
}
