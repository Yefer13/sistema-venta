package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="ubicacion")
public class ubicacion
{
	@Id
	@GeneratedValue
	private int idUbicacion;
	
	@NotEmpty(message= "Escribir la Ciudad de Recidencia")
	private String ciudad;
	
	@NotEmpty(message= "Escribir la Direccion de Recidencia")
	private String direccion;
	
	@NotEmpty(message= "Detallar la Referencia")
	private String referencia;
	
	@OneToOne(mappedBy="tblUbicacion")
	private proveedor tblProveedor;
	
	@OneToOne(mappedBy="tblUbicacion")
	private empleado tblEmpleado;
	
	public int getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(int idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public proveedor getTblProveedor() {
		return tblProveedor;
	}

	public void setTblProveedor(proveedor tblProveedor) {
		this.tblProveedor = tblProveedor;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}
	
	
}
