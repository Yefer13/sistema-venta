package modelo;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detalleServicio")
public class detalleServicio 
{
	@Id
	@GeneratedValue
	private int idDetalleServicio;
	private BigDecimal cantidad;
	private double cantidadP;
	private BigDecimal precioServicio;
	private String caracteristica;
	@ManyToOne
	@JoinColumn(name="idServicios")
	private servicios tblServicios;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade= {CascadeType.MERGE})
	@JoinColumn(name="idServicio")
	private servicio tblServicio;

	public int getIdDetalleServicio() {
		return idDetalleServicio;
	}

	public void setIdDetalleServicio(int idDetalleServicio) {
		this.idDetalleServicio = idDetalleServicio;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public servicios getTblServicios() {
		return tblServicios;
	}

	public void setTblServicios(servicios tblServicios) {
		this.tblServicios = tblServicios;
	}

	public servicio getTblServicio() {
		return tblServicio;
	}

	public void setTblServicio(servicio tblServicio) {
		this.tblServicio = tblServicio;
	}

	public double getCantidadP() {
		return cantidadP;
	}

	public void setCantidadP(double cantidadP) {
		this.cantidadP = cantidadP;
	}

	public BigDecimal getPrecioServicio() {
		return precioServicio;
	}

	public void setPrecioServicio(BigDecimal precioServicio) {
		this.precioServicio = precioServicio;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	
	
}
