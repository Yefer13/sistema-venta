package modelo;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="cliente")
public class cliente
{
	@Id
	@GeneratedValue
	private int idCliente;
	
	@Column(unique=true)
	private String codigo;
	
	@NotEmpty(message= "Escribir un Nombre")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Nombre solo permite letras")
	private String nombres;
	
	@NotEmpty(message = "Escribir el Apellido Paterno")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Apellido Paterno solo permite letras")
	private String apellidoPaterno;
	
	@NotEmpty(message = "Escribir el Apellido Materno")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo Apellido Materno solo permite letras")
	private String apellidoMaterno;
	
	private String dni;
	
	@Email(message="El E-Mail no es valido")
	private String correo;
	
	private String placa;
	private String ruc;
	private String razonSocial;
	private BigDecimal promocion;
	
	@OneToMany(mappedBy="tblCliente")
	private List<venta> tblVenta;
	
	@OneToMany(mappedBy="tblCliente",cascade= {CascadeType.MERGE})
	private List<servicio> tblServicio;

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public List<venta> getTblVenta() {
		return tblVenta;
	}

	public void setTblVenta(List<venta> tblVenta) {
		this.tblVenta = tblVenta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public List<servicio> getTblServicio() {
		return tblServicio;
	}

	public void setTblServicio(List<servicio> tblServicio) {
		this.tblServicio = tblServicio;
	}

	public BigDecimal getPromocion() {
		return promocion;
	}

	public void setPromocion(BigDecimal promocion) {
		this.promocion = promocion;
	}
	
	
}
