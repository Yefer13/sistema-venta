package modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="proveedor")
public class proveedor 
{
	@Id
	@GeneratedValue
	private int idProveedor;
	
	@NotEmpty(message= "Escribir el RUC del Proveedor")
	@Size(min =13 , max=13 , message = "El RUC es de 13 digitos")
	private String ruc;
	
	@NotEmpty(message= "Escribir la Razon Social del Proveedor")
	private String razonSocial;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="idComunicacion")
	private comunicacion tblComunicacion;

	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="idUbicacion")
	private ubicacion tblUbicacion;
	
	@ManyToMany(mappedBy="tblProveedor")
	private List<producto> tblProducto = new ArrayList<>();

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public comunicacion getTblComunicacion() {
		return tblComunicacion;
	}

	public void setTblComunicacion(comunicacion tblComunicacion) {
		this.tblComunicacion = tblComunicacion;
	}

	public ubicacion getTblUbicacion() {
		return tblUbicacion;
	}

	public void setTblUbicacion(ubicacion tblUbicacion) {
		this.tblUbicacion = tblUbicacion;
	}

	public List<producto> getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(List<producto> tblProducto) {
		this.tblProducto = tblProducto;
	}
	
	
}
