package modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="entradaProducto")
public class entradaProducto
{
	@Id
	@GeneratedValue
	private int idEntradaProducto;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToMany(fetch=FetchType.EAGER , mappedBy="tblEntradaProducto",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	private List<detalleEntradaProducto> tblDetalleEntradaProducto;
	
	@ManyToOne
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;

	public int getIdEntradaProducto() {
		return idEntradaProducto;
	}

	public void setIdEntradaProducto(int idEntradaProducto) {
		this.idEntradaProducto = idEntradaProducto;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public List<detalleEntradaProducto> getTblDetalleEntradaProducto() {
		return tblDetalleEntradaProducto;
	}

	public void setTblDetalleEntradaProducto(List<detalleEntradaProducto> tblDetalleEntradaProducto) {
		this.tblDetalleEntradaProducto = tblDetalleEntradaProducto;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}
	
	
	
}
