package modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="empleado")
public class empleado
{
	@Id
	@GeneratedValue
	private int idEmpleado;
	
	@NotEmpty(message= "Numero de DNI Necesario")
	@Size(min = 8 , max=8, message = "El N�mero de DNI es de 8 digitos")
	private String dniEmpleado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name="idCargo")
	private cargo tblCargo;
	
	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="idDatos")
	private datos tblDatos;
	
	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="idUbicacion")
	private ubicacion tblUbicacion;
	
	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="idComunicacion")
	private comunicacion tblComunicacion;
	
	@OneToOne(mappedBy="tblEmpleado")
	private usuario tblUsuario;
	
	@OneToMany(mappedBy="tblEmpleado")
	private List<entradaProducto> tblEntradaProducto;

	@OneToMany(mappedBy="tblEmpleado")
	private List<venta> tblVenta;

	@OneToMany(mappedBy="tblEmpleado")
	private List<servicio> tblServicio;
	
	public int getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public cargo getTblCargo() {
		return tblCargo;
	}

	public void setTblCargo(cargo tblCargo) {
		this.tblCargo = tblCargo;
	}

	public datos getTblDatos() {
		return tblDatos;
	}

	public void setTblDatos(datos tblDatos) {
		this.tblDatos = tblDatos;
	}

	public ubicacion getTblUbicacion() {
		return tblUbicacion;
	}

	public void setTblUbicacion(ubicacion tblUbicacion) {
		this.tblUbicacion = tblUbicacion;
	}

	public comunicacion getTblComunicacion() {
		return tblComunicacion;
	}

	public void setTblComunicacion(comunicacion tblComunicacion) {
		this.tblComunicacion = tblComunicacion;
	}

	public usuario getTblUsuario() {
		return tblUsuario;
	}

	public void setTblUsuario(usuario tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public List<entradaProducto> getTblEntradaProducto() {
		return tblEntradaProducto;
	}

	public void setTblEntradaProducto(List<entradaProducto> tblEntradaProducto) {
		this.tblEntradaProducto = tblEntradaProducto;
	}

	public List<venta> getTblVenta() {
		return tblVenta;
	}

	public void setTblVenta(List<venta> tblVenta) {
		this.tblVenta = tblVenta;
	}

	public String getDniEmpleado() {
		return dniEmpleado;
	}

	public void setDniEmpleado(String dniEmpleado) {
		this.dniEmpleado = dniEmpleado;
	}

	public List<servicio> getTblServicio() {
		return tblServicio;
	}

	public void setTblServicio(List<servicio> tblServicio) {
		this.tblServicio = tblServicio;
	}
	
	
}
