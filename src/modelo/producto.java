package modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="producto")
public class producto 
{
	@Id
	@GeneratedValue
	private int idProducto;
	
	@Lob
	private byte[] img;
	
	@NotEmpty(message= "Escribir un Codigo al Producto")
	@Size(min = 5 , max=5, message = "El Codigo es de 5 digitos")
	@Column(unique=true)
	private String codigoProducto;
	
	@NotEmpty(message= "Escribir un Nombre al Producto")
	private String nombre;
	
	@NotEmpty(message= "Escribir una Marca al Producto")
	private String marca;
	
	@NotEmpty(message= "Detalle la Caracteristica del Producto")
	private String caracteristica;
	
	private BigDecimal stock;
	
	@NotNull(message= "Es Necesario el Precio de Compra")
	private BigDecimal precioCompra;
	
	@NotNull(message= "Es Necesario el Precio de Venta")
	private BigDecimal precioVenta;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToMany(mappedBy="tblProducto")
	private List<detalleVenta> tblDetalleVenta;
	
	@OneToMany(mappedBy="tblProducto")
	private List<detalleEntradaProducto> tblDetalleEntradaProducto = new ArrayList<>();
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name="idCategoria")
	private categoria tblCategoria;
	
	@ManyToMany(fetch=FetchType.EAGER,cascade = {CascadeType.MERGE})
	private List<proveedor> tblProveedor = new ArrayList<>();
	
	public producto()
	{
		
	}

	public producto(int idProducto, String codigoProducto, String nombre, String marca, String caracteristica,
			BigDecimal stock, BigDecimal precioCompra, BigDecimal precioVenta, Date fechaRegistro,
			categoria tblCategoria, List<proveedor> tblProveedor) 
	{
		super();
		this.idProducto = idProducto;
		this.codigoProducto = codigoProducto;
		this.nombre = nombre;
		this.marca = marca;
		this.caracteristica = caracteristica;
		this.stock = stock;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.fechaRegistro = fechaRegistro;
		this.tblCategoria = tblCategoria;
		this.tblProveedor = tblProveedor;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}

	public BigDecimal getStock() {
		return stock;
	}

	public void setStock(BigDecimal stock) {
		this.stock = stock;
	}

	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public categoria getTblCategoria() {
		return tblCategoria;
	}

	public void setTblCategoria(categoria tblCategoria) {
		this.tblCategoria = tblCategoria;
	}

	public List<proveedor> getTblProveedor() {
		return tblProveedor;
	}

	public void setTblProveedor(List<proveedor> tblProveedor) {
		this.tblProveedor = tblProveedor;
	}

	public List<detalleVenta> getTblDetalleVenta() {
		return tblDetalleVenta;
	}

	public void setTblDetalleVenta(List<detalleVenta> tblDetalleVenta) {
		this.tblDetalleVenta = tblDetalleVenta;
	}

	public List<detalleEntradaProducto> getTblDetalleEntradaProducto() {
		return tblDetalleEntradaProducto;
	}

	public void setTblDetalleEntradaProducto(List<detalleEntradaProducto> tblDetalleEntradaProducto) {
		this.tblDetalleEntradaProducto = tblDetalleEntradaProducto;
	}

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}
	
	
	
	
}
