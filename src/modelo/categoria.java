package modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="categoria")
public class categoria 
{
	@Id
	@GeneratedValue
	private int idCategoria;
	@NotEmpty(message= "Escribir una Categoria")
	private String nombre;
	
	@OneToMany(mappedBy="tblCategoria")
	private List<producto> tblProducto;

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<producto> getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(List<producto> tblProducto) {
		this.tblProducto = tblProducto;
	}
	
	
}
