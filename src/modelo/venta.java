package modelo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="venta")
public class venta
{
	@Id
	@GeneratedValue
	private int idVenta;
	private BigDecimal total;
	private BigDecimal numeroVenta;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToMany(fetch=FetchType.EAGER ,mappedBy="tblVenta",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
	private List<detalleVenta> tblDetalleVenta;
	
	@ManyToOne(cascade= {CascadeType.MERGE})
	@JoinColumn(name="idCliente")
	private cliente tblCliente;
	
	@ManyToOne
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;

	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public List<detalleVenta> getTblDetalleVenta() {
		return tblDetalleVenta;
	}

	public void setTblDetalleVenta(List<detalleVenta> tblDetalleVenta) {
		this.tblDetalleVenta = tblDetalleVenta;
	}

	public cliente getTblCliente() {
		return tblCliente;
	}

	public void setTblCliente(cliente tblCliente) {
		this.tblCliente = tblCliente;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}

	public BigDecimal getNumeroVenta() {
		return numeroVenta;
	}

	public void setNumeroVenta(BigDecimal numeroVenta) {
		this.numeroVenta = numeroVenta;
	}

		
}
