package modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "cargo")
public class cargo {
	@Id
	@GeneratedValue
	private int idCargo;
	@NotEmpty(message = "Escribir un Cargo")
	@Pattern(regexp = "^[a-zA-Z������������\\s ]*$", message = "El campo cargo solo permite letras")
	private String nombre;

	@OneToMany(mappedBy = "tblCargo")
	private List<empleado> tblEmpleado;

	public int getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<empleado> getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(List<empleado> tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}

}
