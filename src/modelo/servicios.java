package modelo;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="servicios")
public class servicios 
{
	@Id
	@GeneratedValue
	private int idServicios;
	
	@NotEmpty(message= "Escribir el Nombre del Servicio")
	private String nombre;
	
		
	@OneToMany(mappedBy="tblServicios")
	private List<detalleServicio> tblDetalleServicio;

	public int getIdServicios() {
		return idServicios;
	}

	public void setIdServicios(int idServicios) {
		this.idServicios = idServicios;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	public List<detalleServicio> getTblDetalleServicio() {
		return tblDetalleServicio;
	}

	public void setTblDetalleServicio(List<detalleServicio> tblDetalleServicio) {
		this.tblDetalleServicio = tblDetalleServicio;
	}
	
	
}
