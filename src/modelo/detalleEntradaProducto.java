package modelo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detalleEntradaProducto")
public class detalleEntradaProducto
{
	@Id
	@GeneratedValue
	private int idDetalleEntradaProducto;
	private BigDecimal cantidad;
	
	@ManyToOne
	@JoinColumn(name="idEntradaProducto")
	private entradaProducto tblEntradaProducto;
	
	@ManyToOne
	@JoinColumn(name="idProducto")
	private producto tblProducto;

	public int getIdDetalleEntradaProducto() {
		return idDetalleEntradaProducto;
	}

	public void setIdDetalleEntradaProducto(int idDetalleEntradaProducto) {
		this.idDetalleEntradaProducto = idDetalleEntradaProducto;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public entradaProducto getTblEntradaProducto() {
		return tblEntradaProducto;
	}

	public void setTblEntradaProducto(entradaProducto tblEntradaProducto) {
		this.tblEntradaProducto = tblEntradaProducto;
	}

	public producto getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(producto tblProducto) {
		this.tblProducto = tblProducto;
	}
	
	
}
