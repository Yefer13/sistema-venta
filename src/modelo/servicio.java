package modelo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="servicio")
public class servicio
{
	@Id
	@GeneratedValue
	private int idServicio;
	private BigDecimal total;
	private double cantidad;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToMany(fetch=FetchType.EAGER ,mappedBy="tblServicio",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
	private List<detalleServicio> tblDetalleServicio;
	
	@ManyToOne(cascade= {CascadeType.MERGE})
	@JoinColumn(name="idCliente")
	private cliente tblCliente;

	@ManyToOne
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public List<detalleServicio> getTblDetalleServicio() {
		return tblDetalleServicio;
	}

	public void setTblDetalleServicio(List<detalleServicio> tblDetalleServicio) {
		this.tblDetalleServicio = tblDetalleServicio;
	}

	public cliente getTblCliente() {
		return tblCliente;
	}

	public void setTblCliente(cliente tblCliente) {
		this.tblCliente = tblCliente;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}
	
}
