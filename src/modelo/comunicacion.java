package modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name="comunicacion")
public class comunicacion 
{
	@Id
	@GeneratedValue
	private int idComunicacion;
	
	@NotEmpty(message= "Numero de Celular Necesario - sino Escribir 9 veces 1")
	@Size(min = 9 , max=9, message = "El N�mero de Celular es de 9 digitos")
	private String celular;
	
	@NotEmpty(message= "Si no tiene Telefono Escribir 7 veces 1")
	@Size(min = 7 , max=7, message = "El N�mero de Telefono es de 7 digitos")
	private String telefono;
	
	@Email(message="El E-Mail no es valido")
	private String correo;
	
	@OneToOne(mappedBy="tblComunicacion")
	private proveedor tblProveedor;
	
	@OneToOne(mappedBy="tblComunicacion")
	private empleado tblEmpleado;

	public int getIdComunicacion() {
		return idComunicacion;
	}

	public void setIdComunicacion(int idComunicacion) {
		this.idComunicacion = idComunicacion;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public proveedor getTblProveedor() {
		return tblProveedor;
	}

	public void setTblProveedor(proveedor tblProveedor) {
		this.tblProveedor = tblProveedor;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}
	
	
}
