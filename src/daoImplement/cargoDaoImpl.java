package daoImplement;

import java.util.List;

import daoLocal.cargoDaoLocal;
import modelo.cargo;
import util.conexion;

public class cargoDaoImpl implements cargoDaoLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(cargo c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizar(cargo c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<cargo> lista(cargo c) throws Exception
	{
		cn.abrir_conexion();
		List<cargo> query = cn.em.createQuery("select a from cargo a").getResultList();
		return query;
	}
	
	@Override
	public List<cargo> listaC(String c) throws Exception 
	{
		cn.abrir_conexion();
		List<cargo> query = cn.em.createQuery("select a from cargo a where a.nombre like'%" + c + "%'").getResultList();
		return query;
	}
	
	@Override
	public cargo buscar(String c) throws Exception 
	{
		cn.abrir_conexion();
		cargo cag = new cargo();
		try 
		{
			cn.em.getTransaction().begin();
			cag = cn.em.find(cargo.class, Integer.parseInt(c));
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			cn.cerrar_conexion();
		}
		return cag;
	}
}
