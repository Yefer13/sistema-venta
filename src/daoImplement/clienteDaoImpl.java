package daoImplement;

import java.util.List;

import daoLocal.clienteDaoLocal;
import modelo.cliente;
import util.conexion;

public class clienteDaoImpl implements clienteDaoLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(cliente c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizar(cliente c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public void registrarPro(cliente c) throws Exception
	{
		cn.abrir_conexion();
		cn.em.getTransaction().begin();
		cn.em.persist(c);
		cn.em.getTransaction().commit();
		cn.cerrar_conexion();		
	}
	
	@Override
	public void actualizarPro(cliente c) throws Exception 
	{
			cn.abrir_conexion();
			cn.em.getTransaction().begin();
			cn.em.merge(c);
			cn.em.getTransaction().commit();
			cn.cerrar_conexion();
	}
	
	@Override
	public List<cliente> listar(cliente c) throws Exception 
	{
		cn.abrir_conexion();
		List<cliente> query = cn.em.createQuery("select a from cliente a order by a.idCliente desc").getResultList();
		return query;
	}
	
	@Override
	public List<cliente> listaPro() throws Exception 
	{
		cn.abrir_conexion();
		List<cliente> query = cn.em.createQuery("select a from cliente a where a.promocion >= 5").getResultList();
		return query;
	}
	
	@Override
	public List<cliente> listaC(String c) throws Exception 
	{
		cn.abrir_conexion();
		List<cliente> query = cn.em.createQuery("select a from cliente a where a.nombres like'%" + c + "%'").getResultList();
		return query;
	}
	
	@Override
	public cliente buscar(String c) throws Exception 
	{
		cn.abrir_conexion();
		cliente cli = new cliente();
		try 
		{
			cn.em.getTransaction().begin();
			cli = cn.em.find(cliente.class, Integer.parseInt(c));
			cn.em.getTransaction().commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return cli;
	}
	
	/// para actualizar Servicios y Ventas
	@Override
	public boolean actualizarSV(cliente c) throws Exception
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
}
