package daoImplement;

import java.util.List;

import javax.persistence.Query;

import daoLocal.empleadoDaoLocal;
import modelo.datos;
import modelo.empleado;
import util.conexion;

public class empleadoDaImpl implements empleadoDaoLocal
{
	conexion cn = new conexion();
	
	@Override
	public boolean registrar(empleado em) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(em);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizar(empleado em) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(em);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<empleado> litar(empleado em) throws Exception
	{
		cn.abrir_conexion();
		List<empleado> query = cn.em.createQuery("select a from empleado a order by a.fechaRegistro desc").getResultList();
		return query;
	}
	
	@Override
	public List<empleado> listaE(String em) throws Exception 
	{
		cn.abrir_conexion();
		List<empleado> query = cn.em.createQuery("select e from empleado e, datos d where e.idDatos = d.tblDatos d.nombres like '%"+ em +"%'").getResultList();
		return query;
	}
	
	@Override
	public datos buscarD(String em) throws Exception
	{
		cn.abrir_conexion();
		datos emp = new datos();
		try 
		{
			cn.em.getTransaction().begin();
			emp = cn.em.find(datos.class, Integer.parseInt(em));
			cn.em.getTransaction().commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return emp;
	}
	
	@Override
	public empleado buscar(String em) throws Exception 
	{
		cn.abrir_conexion();
		empleado emp = new empleado();
		try 
		{
			cn.em.getTransaction().begin();
			emp = cn.em.find(empleado.class, Integer.parseInt(em));
			cn.em.getTransaction().commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return emp;
	}
	
	
	@Override
	public List<empleado> listarPorCargos(String m, String n, String o) throws Exception 
	{
		cn.abrir_conexion();
		try
		{
			Query query = cn.em.createQuery("select e,c from empleado e inner join e.tblCargo c where c.nombre =: f or c.nombre =:a or c.nombre =:b ");
			query.setParameter("f", m);
			query.setParameter("a", n);
			query.setParameter("b", o);
			List<empleado> lista = query.getResultList();
			return lista;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
}
