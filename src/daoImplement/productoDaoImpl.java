package daoImplement;

import java.util.List;

import daoLocal.productoDaoLocal;
import modelo.producto;
import util.conexion;

public class productoDaoImpl implements productoDaoLocal
{
	conexion cn = new conexion();
	@Override
	public void registrar(producto p) throws Exception
	{
		cn.abrir_conexion();
		cn.em.getTransaction().begin();
		cn.em.persist(p);
		cn.em.getTransaction().commit();
		cn.cerrar_conexion();
		
	}
	
	@Override
	public void actualizarPro(producto p) throws Exception 
	{
		cn.abrir_conexion();
		cn.em.getTransaction().begin();
		cn.em.merge(p);
		cn.em.getTransaction().commit();
		cn.cerrar_conexion();		
	}
	
	@Override
	public void actualizarS(producto p) throws Exception
	{
		try 
		{
			cn.abrir_conexion();
			cn.em.getTransaction().begin();
			cn.em.merge(p);
			cn.em.getTransaction().commit();
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		
	}
	
	@Override
	public boolean actualizar(producto p) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			
			cn.em.getTransaction().begin();
			cn.em.merge(p);
			cn.em.getTransaction().commit();
			return true;
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<producto> lista(producto p) throws Exception 
	{
		cn.abrir_conexion();
		List<producto> query = cn.em.createQuery("select a from producto a order by a.fechaRegistro desc").getResultList();
		return query;
	}
	
	@Override
	public List<producto> listarP(String p) throws Exception 
	{
		cn.abrir_conexion();
		List<producto> query = cn.em.createQuery("select a from producto a where a.nombre like '%" + p + "%'").getResultList();
		return query;
	}
	
	@Override
	public producto buscar(String p) throws Exception 
	{
		cn.abrir_conexion();
		producto pro = new producto();
		try 
		{
			cn.em.getTransaction().begin();
			pro = cn.em.find(producto.class, Integer.parseInt(p));
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			cn.cerrar_conexion();
		}
		return pro;
	}
	
	@Override
	public List<producto> listaPS() throws Exception 
	{
		cn.abrir_conexion();
		List<producto> query = cn.em.createQuery("select a from producto a where a.stock <= 5").getResultList();
		return query;
	}
}
