package daoImplement;

import java.util.List;

import daoLocal.serviciosDaoLocal;
import modelo.producto;
import modelo.servicios;
import util.conexion;

public class serviciosDaoImpl implements serviciosDaoLocal
{
	conexion cn = new conexion();
	
	@Override
	public boolean registrar(servicios se) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(se);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizar(servicios se) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(se);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<servicios> listar(servicios se) throws Exception 
	{
		cn.abrir_conexion();
		List<servicios> query = cn.em.createQuery("select a from servicios a").getResultList();
		return query;
	}
	
	@Override
	public List<servicios> listarS(String se) throws Exception 
	{
		cn.abrir_conexion();
		List<servicios> query = cn.em.createQuery("select a from servicios a where a.nombre like '%" + se + "%'").getResultList();
		return query;
	}
	
	@Override
	public servicios buscar(String se) throws Exception 
	{
		cn.abrir_conexion();
		servicios ser = new servicios();
		try 
		{
			cn.em.getTransaction().begin();
			ser = cn.em.find(servicios.class, Integer.parseInt(se));
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return ser;
	}
}
