package daoImplement;

import java.util.List;

import daoLocal.proveedorDaoLocal;
import modelo.proveedor;
import util.conexion;

public class proveedorDaoImpl implements proveedorDaoLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(proveedor p) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(p);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizar(proveedor p) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(p);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<proveedor> lista(proveedor p) throws Exception 
	{
		cn.abrir_conexion();
		List<proveedor> query = cn.em.createQuery("select a from proveedor a order by a.fechaRegistro desc").getResultList();
		return query;
	}
	
	@Override
	public List<proveedor> listaP(String p) throws Exception 
	{
		cn.abrir_conexion();
		List<proveedor> query = cn.em.createQuery("select a from proveedor a where a.razonSocial like '%" + p + "%'").getResultList();
		return query;
	}
	
	@Override
	public proveedor buscar(String p) throws Exception 
	{
		cn.abrir_conexion();
		proveedor prov = new proveedor();
		try 
		{
			cn.em.getTransaction().begin();
			prov = cn.em.find(proveedor.class, Integer.parseInt(p));
			cn.em.getTransaction().commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return prov;
	}
}
