package daoImplement;

import java.util.List;

import javax.persistence.Query;

import modelo.usuario;
import util.conexion;
import daoLocal.usuarioDaoLocal;

public class usuarioDaoImpl implements usuarioDaoLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(usuario usua) throws Exception
	{
		try
		{
			cn.em.getTransaction().begin();
			cn.em.persist(usua);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return  false;
		}
		finally {
			cn.cerrar_conexion();
		}
		
	}
	
	@Override
	public boolean actualizar(usuario usua) throws Exception
	{
		cn.abrir_conexion();
		try
		{
			cn.em.getTransaction().begin();
			cn.em.merge(usua);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return  false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<usuario> lista(usuario usua) throws Exception 
	{
		cn.abrir_conexion();
		List<usuario> query = cn.em.createQuery("select a from usuario a ORDER BY a.idUsuario desc").getResultList();
		return query;
	}
	
	@Override
	public usuario validar(usuario usua) throws Exception 
	{
		cn.abrir_conexion();
		usuario usu = new usuario();
		try 
		{
			Query query = cn.em.createQuery("select a from usuario a where a.usuario = :p1 and a.contrasena = :p2");
			query.setParameter("p1", usua.getUsuario());
			query.setParameter("p2", usua.getContrasena());
			List<usuario> lista = query.getResultList();
			if(lista.size()>0)
			{
				usu = lista.get(0);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return usu;
	}
}
