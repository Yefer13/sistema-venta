package daoImplement;

import java.util.List;

import javax.persistence.Query;

import com.sun.mail.util.QEncoderStream;

import daoLocal.ventaDaoLocal;
import modelo.cliente;
import modelo.detalleVenta;
import modelo.venta;
import util.conexion;

public class ventaDaoImpl implements ventaDaoLocal
{
	
	conexion cn = new conexion();
	@Override
	public boolean registrar(venta v) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(v);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<venta> lista() throws Exception
	{
		// TODO Auto-generated method stub
		cn.abrir_conexion();
		try 
		{
			List<venta> lista = cn.em.createQuery("select a from venta a order by a.fechaRegistro desc").getResultList();
			return lista;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public void actualizar(venta v) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(v);
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<venta> listar(venta v) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<venta> listaCP(cliente cli) throws Exception 
	{
		cn.abrir_conexion();
		List<venta> lista = null;
		try 
		{
			Query query = cn.em.createQuery("select a from venta a, cliente c where a.tblCliente = c.idCliente and c.nombres = :p1 ORDER BY a.fechaRegistro desc ");
			query.setParameter("p1", cli.getNombres());
			lista = query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lista;
	}
	
	
	@Override
	public void actualizarDet(detalleVenta d) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(d);
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally {
			cn.cerrar_conexion();
		}		
	}
	
	@Override
	public boolean actualizarV(venta v) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(v);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean eliminar(venta v) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			v = cn.em.find(venta.class, v.getIdVenta());
			cn.em.getTransaction().begin();
			cn.em.remove(v);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	
	
	////OJO FALTA TERMINARLO////
	@SuppressWarnings("unchecked")
	@Override
	public venta obtenerUltimoRegistro() throws Exception 
	{
		cn.abrir_conexion();
		List<venta> lista = null;
		try 
		{
			Query query = cn.em.createQuery("select a from venta a order by a.idVenta desc");
			lista = (List<venta>) query.setMaxResults(1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return (venta) lista;
	}
	
	
	@Override
	public Long obtenerTotalRegistro() throws Exception
	{
//		cn.abrir_conexion();
//		String hql = "select count(*) from venta";
//		Query q = cn.em.createQuery(hql);
//		return (long) q.getMaxResults();
		cn.abrir_conexion();
		
		try 
		{
			Query query = cn.em.createQuery("select count(a) from venta a ");
			query.getResultList();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return Long.MAX_VALUE;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<venta> listaUltimoRegistro() throws Exception
	{
		cn.abrir_conexion();
		List<venta> lista = null;
		try 
		{
			Query query = cn.em.createQuery("select a from venta a order by a.idVenta desc");
			lista = (List<venta>) query.setMaxResults(1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<venta> listaTotalRegistro() throws Exception 
	{
		cn.abrir_conexion();
		List<venta> lista = null;
		try 
		{
			Query query = cn.em.createQuery("select count(a) from venta a");
			lista = (List<venta>) query.getResultList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return lista;
	}
}
