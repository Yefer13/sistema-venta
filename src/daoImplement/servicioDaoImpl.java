package daoImplement;

import java.util.List;

import javax.persistence.Query;

import daoLocal.servicioDaoLocal;
import modelo.cliente;
import modelo.detalleServicio;
import modelo.servicio;
import util.conexion;

public class servicioDaoImpl  implements servicioDaoLocal
{
	conexion cn = new conexion();
	
	@Override
	public boolean registrar(servicio s) throws Exception
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(s);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public void actualizar(servicio s) throws Exception
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(s);
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<servicio> lista(servicio s) throws Exception 
	{
		cn.abrir_conexion();
		List<servicio> query = cn.em.createQuery("select a from servicio a order by a.fechaRegistro desc").getResultList();
		return query;
	}
	
	@Override
	public List<servicio> listaCS(cliente c) throws Exception 
	{
		cn.abrir_conexion();
		List<servicio> lista = null;
		try 
		{
			Query query = cn.em.createQuery("select a from servicio a, cliente c where a.tblCliente = c.idCliente and c.nombres = :p1 ORDER BY a.fechaRegistro desc ");
			query.setParameter("p1", c.getNombres());
			lista = query.getResultList();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lista;
	}
	
	@Override
	public boolean actualizarS(servicio s) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(s);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean eliminar(servicio s) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			s = cn.em.find(servicio.class, s.getIdServicio());
			cn.em.getTransaction().begin();
			cn.em.remove(s);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	////DETALLE SERVICIO/////
	
	@Override
	public boolean eliminarD(detalleServicio d) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			detalleServicio de = cn.em.find(detalleServicio.class, d.getIdDetalleServicio());
			cn.em.getTransaction().begin();
			cn.em.remove(de);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public boolean actualizarD(detalleServicio d) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(d);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public void eliminarDD(detalleServicio d) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			d = cn.em.find(detalleServicio.class, d.getIdDetalleServicio());
			cn.em.getTransaction().begin();
			cn.em.remove(d);
			cn.em.getTransaction().commit();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally {
			cn.cerrar_conexion();
		}
	}
}
