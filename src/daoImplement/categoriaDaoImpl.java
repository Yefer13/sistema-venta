package daoImplement;

import java.util.List;

import daoLocal.categoriaDaoLocal;
import modelo.categoria;
import util.conexion;

public class categoriaDaoImpl implements categoriaDaoLocal
{
	conexion cn = new conexion();
	
	@Override
	public boolean registrar(categoria c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		
	}
	
	@Override
	public boolean actualizar(categoria c) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(c);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<categoria> lista(categoria c) throws Exception 
	{
		cn.abrir_conexion();
		List<categoria> query = cn.em.createQuery("select a from categoria a").getResultList();
		return query;
	}
	
	@Override
	public List<categoria> listaC(String c) throws Exception 
	{
		cn.abrir_conexion();
		List<categoria> query = cn.em.createQuery("select a from categoria a where a.nombre like '%" + c + "%'").getResultList();
		return query;
	}
	
	@Override
	public categoria buscar(String c) throws Exception
	{
		cn.abrir_conexion();
		categoria cat = new categoria();
		try 
		{
			cn.em.getTransaction().begin();
			cat = cn.em.find(categoria.class, Integer.parseInt(c));
			cn.em.getTransaction().commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.cerrar_conexion();
		}
		return cat;
	}
	
}
