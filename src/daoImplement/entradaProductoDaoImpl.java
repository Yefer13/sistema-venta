package daoImplement;

import java.util.List;
import daoLocal.entradaProductoDaoLocal;
import modelo.entradaProducto;
import util.conexion;

public class entradaProductoDaoImpl implements entradaProductoDaoLocal 
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(entradaProducto enPro) throws Exception
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(enPro);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public void registrarV(entradaProducto enPro) throws Exception
	{
		cn.abrir_conexion();
		cn.em.getTransaction().begin();
		cn.em.persist(enPro);
		cn.em.getTransaction().commit();
		cn.cerrar_conexion();
	}
	
	@Override
	public boolean actualizar(entradaProducto enPro) throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(enPro);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
	
	@Override
	public List<entradaProducto> listar() throws Exception 
	{
		cn.abrir_conexion();
		try 
		{
			List<entradaProducto> lista = cn.em.createQuery("select a from entradaProducto a order by a.fechaRegistro desc").getResultList();
			return lista;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.cerrar_conexion();
		}
	}
}
