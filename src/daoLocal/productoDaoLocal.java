package daoLocal;

import java.util.List;

import modelo.producto;

public interface productoDaoLocal
{
	public abstract void registrar(producto p) throws Exception;
	public abstract List<producto> lista(producto p) throws Exception;
	public abstract void actualizarS(producto p) throws Exception;/// este actualizar sirve para entrada y venta de productos///
	/// buscar ///
	public abstract List<producto> listarP(String p) throws Exception;
	public abstract producto buscar(String p) throws Exception;
	
	///stock menor que 5 ///
	public abstract List<producto> listaPS() throws Exception;
	
	////actualizar el producto ///
	
	public abstract boolean actualizar(producto p) throws Exception;
	public abstract void actualizarPro(producto p) throws Exception;
}
