package daoLocal;

import java.util.List;

import modelo.datos;
import modelo.empleado;

public interface empleadoDaoLocal
{
	public abstract boolean registrar(empleado em) throws Exception;
	public abstract boolean actualizar(empleado em) throws Exception;
	public abstract List<empleado> litar(empleado em) throws Exception;
	
	////buscar///
	public abstract empleado buscar(String em) throws Exception;
	public abstract List<empleado> listaE(String em) throws Exception;
	
	public abstract datos buscarD(String em) throws Exception;
	
	////lista por cargos////
	public List<empleado> listarPorCargos(String m, String n, String o) throws Exception;
}
