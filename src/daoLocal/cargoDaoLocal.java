package daoLocal;

import java.util.List;

import modelo.cargo;

public interface cargoDaoLocal 
{
	public abstract boolean registrar(cargo c) throws Exception;
	public abstract boolean actualizar(cargo c) throws Exception;
	public abstract List<cargo> lista(cargo c) throws Exception;
	
	////buscar///
	public abstract List<cargo> listaC(String c) throws Exception;
	public abstract cargo buscar(String c) throws Exception;
}
