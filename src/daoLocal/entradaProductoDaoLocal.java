package daoLocal;

import java.util.List;

import modelo.entradaProducto;

public interface entradaProductoDaoLocal
{
	public abstract boolean registrar(entradaProducto enPro) throws Exception;
	public abstract boolean actualizar(entradaProducto enPro) throws Exception;
	public  List<entradaProducto> listar() throws Exception;
	
	public abstract void registrarV(entradaProducto enPro) throws Exception;
}
