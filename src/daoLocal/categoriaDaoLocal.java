package daoLocal;

import java.util.List;

import modelo.categoria;

public interface categoriaDaoLocal 
{
	public abstract boolean registrar(categoria c) throws Exception;
	public abstract boolean actualizar(categoria c) throws Exception;
	public abstract List<categoria> lista(categoria c) throws Exception;
	
	///buscar///
	public abstract List<categoria> listaC(String c) throws Exception;
	public abstract categoria buscar(String c) throws Exception;
 }
