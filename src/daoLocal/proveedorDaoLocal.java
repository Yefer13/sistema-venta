package daoLocal;

import java.util.List;

import modelo.proveedor;

public interface proveedorDaoLocal 
{
	public abstract boolean registrar(proveedor p) throws Exception;
	public abstract boolean actualizar(proveedor p) throws Exception;
	public abstract List<proveedor> lista(proveedor p) throws Exception;
	
	//para buscar//
	public abstract List<proveedor> listaP(String p) throws Exception;
	public abstract proveedor buscar(String p) throws Exception;
}
