package daoLocal;

import java.util.List;

import modelo.cliente;
import modelo.detalleVenta;
import modelo.venta;

public interface ventaDaoLocal
{
	public abstract boolean registrar(venta v) throws Exception;
	public abstract void actualizar(venta v) throws Exception;
	public abstract List<venta> listar(venta v) throws Exception;
	
	public List<venta> lista() throws Exception;
	
	///listar cliente productos //
	
	public List<venta> listaCP(cliente cli) throws Exception; 
	
//// detalleventaActualizar///
	
	public abstract void actualizarDet(detalleVenta d) throws Exception;
	
	//// solo venta 
	
	public abstract boolean actualizarV(venta v) throws Exception;
	public abstract boolean eliminar(venta v) throws Exception;
	
	////generar numero de venta ///
	
	public venta obtenerUltimoRegistro() throws Exception;
	public Long obtenerTotalRegistro() throws Exception;
	
	public abstract List<venta> listaUltimoRegistro() throws Exception;
	public abstract List<venta> listaTotalRegistro() throws Exception;
} 
