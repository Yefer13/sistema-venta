package daoLocal;

import java.util.List;

import modelo.usuario;

public interface usuarioDaoLocal 
{
	public abstract boolean registrar(usuario usua) throws Exception;
	public abstract boolean actualizar(usuario usua) throws Exception;
	public List<usuario> lista(usuario usua) throws Exception;
	public abstract usuario validar (usuario usua) throws Exception;
	
}
