package daoLocal;

import java.util.List;

import modelo.servicios;

public interface serviciosDaoLocal
{
	public abstract boolean registrar(servicios se) throws Exception;
	public abstract boolean actualizar(servicios se) throws Exception;
	public abstract List<servicios> listar(servicios se) throws Exception;
	
	//// buscar //
	
	public abstract List<servicios> listarS(String se) throws Exception;
	public abstract servicios buscar(String se) throws Exception;
}
