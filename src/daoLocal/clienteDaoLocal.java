package daoLocal;

import java.util.List;

import modelo.cliente;

public interface clienteDaoLocal 
{
	public abstract boolean registrar(cliente c) throws Exception;
	public abstract boolean actualizar(cliente c) throws Exception;
	public abstract void actualizarPro(cliente c) throws Exception;
	public abstract void registrarPro(cliente c) throws Exception;
	public abstract List<cliente> listar(cliente c) throws Exception;
	///buscar///
	
	public abstract List<cliente> listaC(String c) throws Exception;
	public abstract cliente buscar(String c) throws Exception;
	
	/// lista promocion ///
	
	public abstract List<cliente> listaPro() throws Exception;
	
	//// Para Actualizar Servicios y Ventas 
	public abstract boolean actualizarSV(cliente c) throws Exception;
}
