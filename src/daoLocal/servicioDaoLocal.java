package daoLocal;

import java.util.List;

import modelo.cliente;
import modelo.detalleServicio;
import modelo.servicio;

public interface servicioDaoLocal
{
	public abstract  boolean registrar(servicio s) throws Exception;
	public abstract void actualizar(servicio s) throws Exception;
	public abstract List<servicio> lista(servicio s) throws  Exception;
	
	
	/// listar cliente servicio ///
	public List<servicio> listaCS(cliente c) throws Exception;
	
	public abstract boolean actualizarS(servicio s) throws Exception;
	
	public abstract boolean eliminar(servicio s) throws Exception;
	
	
	///// DETALLE SERVICIOS ///
	public abstract boolean actualizarD(detalleServicio d) throws Exception;
	public abstract boolean eliminarD(detalleServicio d) throws Exception;
	
	public abstract void eliminarDD(detalleServicio d) throws Exception;
}
